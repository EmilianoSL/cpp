#include <vector>
#include <tuple>
#include <memory>
#include <map>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <ctime>
#include "math.h"
#include <chrono>
#include <algorithm>



#include "helpers/timehelper.h"
#include "helpers/conventionshelper.h"
#include "helpers/matrixhelper.h"
#include "helpers/regressionhelper.h"
#include "helpers/InterfaceCLAPACK.h"
#include "helpers/Grid.h"
#include "helpers/csv.h"
#include "helpers/objfun.h"
#include "helpers/neldermead.h"
#include "Portfolio/Bucket.h"
#include "Portfolio/OneCashFlowFix.h"
#include "Portfolio/Leg.h"
#include "Portfolio/Bond.h"
#include "Portfolio/Portfolio.h"
#include "Portfolio/Instrument.h"
#include "TermStructure/TermStructure.h"
#include "TermStructure/TermStructureFlat.h"
#include "TermStructure/TermStructureParametric.h"
#include "Market/BondReference.h"
#include "Market/DailyCash.h"
//#include "opt/Constraints.h"
//#include "opt/CostFunction.h"
//#include "opt/Problem.h"

#include <blitz/array.h>
#include <boost/make_shared.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>
#define BOOST_DATE_TIME_NO_LIB
#include <boost/date_time.hpp>

int main(int argc, char *argv[]) {

  std::cout << std::setprecision(4) << std::fixed;
  std::cout << "==========================BEGIN==========================" <<std::endl;

  // Config Parameters
  const int n = 2; // dimension of the opt problem
  const	double reqmin = 1.0E-08;
  const int kcount = 500;
  const int konvge = 500;
  const int level = 2;
  const int maxiter = 10;
  const double eps = 10e-4;
  const int min_bonds = 11;
  const bool nelder = false;

  /************************************************************************
   ************************************************************************/
  // Load Data
  std::cout << std::setprecision(2) << std::fixed;
  std::string filename1 = "data/bondref.csv";
  std::string filename2 = "data/dailycash.csv";

  BondReference BondReference("dummy_string");
  std::cout << "Reading BondReference File ..." << std::endl;
  BondReference.makeMapOfBonds(filename1);
  std::cout << "BondReference File OK" << std::endl;
  //BondReference.printMapOfBonds();

  DailyCash DailyCash("dummy_string");
  std::cout << "Reading DailyCash File ..." << std::endl;
  DailyCash.makeMapOfDailyCash(filename2);
  std::cout << "DailyCash File OK" << std::endl;
  //DailyCash.printMapOfDailyCash();

   // Create Map of Bonds
  auto map_cash = DailyCash.getMapOfDailyCash();
  std::cout << "map_cash OK" << std::endl;
  auto map_bonds = BondReference.getMapOfBonds();
  std::cout << "map_bonds OK" << std::endl;

  std::set<boost::gregorian::date> dates_set = DailyCash.getSetOfUniqueDates();

  std::vector<Bond> bonds_vector;
  for (auto& element : map_cash){
    if ( map_bonds.count(element.first.second) == 1 ) { // search for ISIN
      Bond bond(element.second.getRefDate(),
  		element.second.getBondCode(),
  		element.second.getWgtPrice(),
  		element.second.getAccruedInt(),
  		map_bonds.find(element.first.second)->second.getCouponRate(),
  		map_bonds.find(element.first.second)->second.getCouponFreq(),
  		map_bonds.find(element.first.second)->second.getCurrency(),
  		map_bonds.find(element.first.second)->second.getCountry(),
  		map_bonds.find(element.first.second)->second.getDayCount(),
  		map_bonds.find(element.first.second)->second.getMaturityDate(),
  		map_bonds.find(element.first.second)->second.getLastCouponDate()
  		);
      std::string country = map_bonds.find(element.first.second)->second.getCountry();
      if ( country.compare("DE") == 0 || country.compare("AT") == 0 || country.compare("BE") == 0 || country.compare("NL") == 0) {
   	if (bond.getPrice() < 102.0 && bond.getPrice() > 98.0 ) bonds_vector.push_back(bond);
      }
    }
  }

   std::vector<std::pair<boost::gregorian::date,int>> bonds_order;
   for (int i=0 ; i<bonds_vector.size() ; i++){
     auto pair = std::make_pair(bonds_vector[i].getMaturityDate(),i);
     bonds_order.push_back(pair);
   }

   std::sort(bonds_order.begin(), bonds_order.end());

   std::vector<Bond> bonds_vector_ordered;

   for (int i =0; i<bonds_order.size(); i++){
     bonds_vector_ordered.push_back(bonds_vector[bonds_order[i].second]);
   }

   std::vector<Portfolio> portfolios;
   std::set<boost::gregorian::date>::iterator it_date;
   for (it_date = dates_set.begin(); it_date != dates_set.end(); it_date++){
     Portfolio portfolio(*it_date);
     for (int i = 0; i < bonds_vector_ordered.size() ; i++){
       if ( *it_date == bonds_vector_ordered[i].getReferenceDate() ){
	 portfolio.setOneBond(bonds_vector_ordered[i]);
       }
     }
     portfolios.push_back(portfolio);
   } 
    
  auto t = std::time(nullptr);
  auto m = *std::localtime(&t);

  std::ostringstream oss;
  oss << std::put_time(&m,"%d-%m-%Y_%Hh%Mm");
  auto str = oss.str();

  boost::filesystem::path full_path( boost::filesystem::current_path() );
  std::string out_fname = "bonds_all_" + str  + ".csv";
  std::string out_fname2 = "results_" + str  + ".csv";
  std::string out_full_name = full_path.string() + "/out/" + out_fname;
  std::string out_full_name2 = full_path.string() + "/out/" + out_fname2;
  std::ofstream outfile(out_full_name);
  std::ofstream outfile2(out_full_name2);

  outfile << "Date" << ";"
	  << "BondCode" << ";"
          << "ResidualMaturity" << ";"
          << "YieldToMaturity"  << ";"
	  << "MaturityBracket"  << ";"
	  << "BracketMeanYield" << ";"
	  <<  "LowerBound"      << ";"
	  << "UpperBound"       << std::endl;

  outfile2 << "Date" << ";"
	   << "Method" << ";"
	   << "NumBonds" << ";"
	   << "Seconds" << ";"
	   << "Level" << ";"
	   << "IterNewton" << ";"
	   << "CostFunction" << ";"
	   << "RMSE" << ";"
	   << "MAD" << ";"
	   << "x1" << ";"
	   << "x2" << ";"
	   << "x3" << ";"
	   << "x4" << ";"
	   << "x5" << ";"
	   << "x6" << std::endl;
  
  std::vector<std::pair<double,double>> brackets;
  brackets.push_back(std::make_pair(0.0,0.25));
  brackets.push_back(std::make_pair(0.25,1.0));
  brackets.push_back(std::make_pair(1.0,2.0));
  brackets.push_back(std::make_pair(2.0,3.0));
  brackets.push_back(std::make_pair(3.0,4.0));
  brackets.push_back(std::make_pair(4.0,5.0));
  brackets.push_back(std::make_pair(5.0,6.0));
  brackets.push_back(std::make_pair(6.0,7.0));
  brackets.push_back(std::make_pair(7.0,8.0));
  brackets.push_back(std::make_pair(8.0,9.0));
  brackets.push_back(std::make_pair(9.0,10.0));
  brackets.push_back(std::make_pair(10.0,15.0));
  brackets.push_back(std::make_pair(15.0,20.0));
  brackets.push_back(std::make_pair(20.0,25.0));
  brackets.push_back(std::make_pair(25.0,30.0));
  brackets.push_back(std::make_pair(30.0,99.0));

  
  for(int i=0 ; i < portfolios.size() ; i++){
    const auto x = portfolios[i].makeTimeToMaturities();
    const auto y = portfolios[i].getPortfolioYields();
    const auto b = portfolios[i].makeMaturityBrackets(brackets);
    const auto m = portfolios[i].makeMapYieldByBracket(brackets);
    for (int j=0; j < x.rows()-1 ; ++j){
      outfile  << portfolios[i].getReferenceDate() << ";"
	       << portfolios[i].getOneInstrument(j)->getBondCode() << ";"
  	       << x(j) << ";"
               << y(j) << ";"
  	       << b(j) << std::endl;
    }
  }
   
  outfile.close();
  // Remove Bonds with t>30 or t<0.25
  std::string out_fname_2 = "bonds_erased_maturity_" + str  + ".csv";
  std::string out_full_name_2 = full_path.string() + "/out/" + out_fname_2;
  std::ofstream outfile_2(out_full_name_2);
  outfile_2 << "Date" << ";"
	   << "BondCode" << ";"
	   << "ResidualMaturity" << ";"
	   << "YieldToMaturity" << std::endl;
  for(int i=0 ; i < portfolios.size()  ; i++) portfolios[i].removeOutliers(outfile_2);
  outfile_2.close();

  // Remove Bonds with yield >< mean +- 2*std
  std::string out_fname_ = "bonds_erased_yield_" + str  + ".csv";
  std::string out_full_name_ = full_path.string() + "/out/" + out_fname_;
  std::ofstream outfile_(out_full_name_);
  outfile_ << "Date" << ";"
	  << "BondCode" << ";"
	  << "ResidualMaturity" << ";"
	  << "YieldToMaturity" << ";"
	  << "MaturityBracket" << ";"
          << "LowerBound" << ";"
          << "UpperBound" << std::endl;
  for(int i=0 ; i < portfolios.size() ; i++){ 
    int n = portfolios[i].getPortfolioSize();
    int m = 0;
    while (n!=m) {
      n = portfolios[i].getPortfolioSize();
      portfolios[i].removeOutliers(brackets,outfile_);
      m = portfolios[i].getPortfolioSize();
    } 
  }

  std::string out_fname3 = "bonds_fitted_" + str  + ".csv";
  std::string out_full_name3 = full_path.string() + "/out/" + out_fname3;
  std::ofstream outfile_fit(out_full_name3);
  outfile_fit << "Date" << ";"
	      << "BondCode" << ";"
	      << "ResidualMaturity" << ";"
	      << "MaturityBracket" << ";"
	      << "YieldToMaturity" << ";"
	      << "FittedYield" << ";"
	      << "Price" << ";"
	      << "FittedPrice" << ";"	    
	      << "PriceDiff-Abs" << std::endl;
	      
  // Calibrate Svensson Discount Curve for each day
  std::vector<blitz::Array<double,2> > x_opt_vec;
  for (int i=0;i<portfolios.size();++i){
    auto start_time =  std::chrono::system_clock::now();
    std::cout << "Calibrating " << portfolios[i].getReferenceDate() <<  " ..." << std::endl;
    const boost::gregorian::date start_date = portfolios[i].getReferenceDate();
    blitz::Array<double,2> x(6,1);
    if (i==0) {
      x = 0.01,0.05,0.1,0.01,1.0,2.0;
      x_opt_vec.push_back(x);}
    else {
      x = x_opt_vec[i-1];
    }
    auto results = portfolios[i].fitBondDiscountCurve(outfile_fit,x,level);
    auto x_opt = std::get<2>(results);

   
    const boost::gregorian::date end_date = timehelper::addToDate(start_date, 30, timehelper::YEAR);
    const CurveType curve_type = TermStructure::getCurveTypeFromString("zero");
    const ParametricForm param_form = TermStructureParametric::getParametricFormFromString("Svensson");
    const std::string day_count = "ACTUAL_365";

    TermStructureParametric term_structure(start_date, end_date, x_opt, param_form, curve_type,day_count);
    blitz::Array<double,2> x_opt_final(6,1);
    if (nelder) {
      auto matrices = portfolios[i].makeAllMatrices(); // P, T, C, W, D
      const auto P = std::get<0>(matrices);
      const auto T = std::get<1>(matrices);
      const auto C = std::get<2>(matrices);
      const auto W = std::get<3>(matrices);
      const auto D = std::get<4>(matrices);
      double ynewlow;
      int icount;
      int ifault;	
      int numres;

      double *start;
      double *step;
      double *xmin;
     

      start = new double[n];
      step = new double[n];
      xmin = new double[n];

      start[0] = x_opt(0) ;
      start[1] = x_opt(1);
      start[2] = x_opt(2);
      start[3] = x_opt(3);
      start[4] = x_opt(4);
      start[5] = x_opt(5);
      ynewlow = objfuncon(start,portfolios[i], C, W, T, P,
			  start_date, end_date, curve_type, param_form, day_count);

      nelmin ( objfuncon,portfolios[i], C, W, T, P,
	       start_date, end_date, curve_type, param_form, day_count,
	       n, start, xmin, &ynewlow, reqmin, step,
	       konvge, kcount, &icount, &numres, &ifault );

      x_opt_final = xmin[0],xmin[1],xmin[2],xmin[3],xmin[4],xmin[5];
      x_opt_vec.push_back(x_opt_final);
    }
    
   
    if (!nelder) {    
      x_opt_final = x_opt;
      x_opt_vec.push_back(x_opt_final);
    }
    
    TermStructureParametric term_structure_opt(start_date, end_date, x_opt_final, param_form, curve_type,day_count);
    blitz::Array<double,1> diff_array = portfolios[i].makePricesDiff(term_structure_opt);
    blitz::Array<double,1> bond_fitted = portfolios[i].matrixPresentValue(term_structure_opt);
    blitz::Array<double,1> weights(n);
    blitz::Array<double,2> W = portfolios[i].makeWeightMatrixD();
  
    blitz::Array<std::string,1> B = portfolios[i].makeMaturityBrackets(brackets);
    for (int i=0; i<n; i++) weights(i) = W(i,i);
    double F = portfolios[i].calcCostFunctionValue(W,diff_array);
    double mad  = sum(fabs(diff_array)*weights);
    double rmse = sqrt(sum(diff_array*diff_array*weights)) ;
    auto end = std::chrono::system_clock::now();
    auto diff = end - start_time;
    auto seconds =  std::chrono::duration_cast<std::chrono::seconds>(diff).count();

    
    
    double x_1,x_2,x_3,x_4,x_5,x_6;
    x_1 = x_opt_final(0) < 0.0000001 ? 0.0 : x_opt_final(0);
    x_2 = x_opt_final(1) < 0.0000001 ? 0.0 : x_opt_final(1);
    x_3 = x_opt_final(2) < 0.0000001 ? 0.0 : x_opt_final(2);
    x_4 = x_opt_final(3) < 0.0000001 ? 0.0 : x_opt_final(3);
    x_5 = x_opt_final(4) < 0.0000001 ? 0.0 : x_opt_final(4);
    x_6 = x_opt_final(5) < 0.0000001 ? 0.0 : x_opt_final(5);
   
    outfile2  << portfolios[i].getReferenceDate() << ";"
	      << "HCP-Newton" << ";"
	      << portfolios[i].getPortfolioSize() << ";"
	      << seconds << ";"
	      << level << ";"
	      << std::get<0>(results) << ";"
	      << F << ";"
	      << rmse << ";"
	      << mad << ";"
	      << x_1 << ";"
	      << x_2 << ";"
	      << x_3 << ";"
	      << x_4 << ";"
	      << x_5 << ";"
	      << x_6 << std::endl;

    for (int k=0;k<portfolios[i].getPortfolioSize();++k){
      double bond_price  = portfolios[i].getOneInstrument(k)->getPrice();
      double fitted_price = portfolios[i].getOneInstrument(k)->calculatePresentValue(term_structure_opt);
       outfile_fit << portfolios[i].getReferenceDate() << ";"
		   << portfolios[i].getOneInstrument(k)->getBondCode() << ";"
		   << portfolios[i].getOneInstrument(k)->calculateTimeToMaturity() << ";"
		   << B(k) << ";"
		   << portfolios[i].getOneInstrument(k)->getYield() << ";"
		   << portfolios[i].getOneInstrument(k)->calculateYield(fitted_price) << ";"
		   << bond_price<< ";"
		   << fitted_price << ";"	
		   << abs(bond_price - fitted_price) << std::endl;
    }
   
    
  } // for portfolios
   outfile2.close();
   outfile_fit.close();
   
   std::cout << "==========================END============================" <<std::endl;
  return 0;
} // main
