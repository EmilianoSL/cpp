#ifndef DAILYCASH_H_
#define DAILYCASH_H_

#include <string>
#include <vector>
#include <memory>
#include <map>
#include <set>

#include "../helpers/csv.h"


#define BOOST_DATE_TIME_NO_LIB
#include <boost/date_time.hpp>

class DailyCash{
 public:
/** @brief Constructor */
  DailyCash(
	    const boost::gregorian::date& ref_date,
	    const std::string& bond_code,
	    const std::string& bond_type,
	    const std::string& market_code,
	    const double ref_price,
	    const double mid_yield,
	    const double max_price,
	    const double min_price,
	    const double wgt_price,
	    const double accrued_int,
	    const boost::gregorian::date& settl_date,
	    const int part_number,
	    const int num_oblg,
	    const double total_volume,
	    const double avg_size,
	    const double avg_spread,
	    const int imb_trade);

  DailyCash(const std::string& dummy_string);
	    
/** @brief Destructor */
  virtual ~DailyCash();

/** @brief Getter methods */
  boost::gregorian::date getRefDate() const;
  std::string getBondCode() const;
  std::string getBondType() const;
  std::string getMarketCode() const;
  double getRefPrice() const;
  double getMidYield() const;
  double getMaxPrice() const;
  double getMinPrice() const;
  double getWgtPrice() const;
  double getAccruedInt() const;
  boost::gregorian::date getSettlDate() const;
  int getPartNumber() const;
  int getNumOblg() const;
  double getTotalVolume() const;
  double getAvgSize() const;
  double getAvgSpread() const;
  int getImbTrade() const;

  void makeMapOfDailyCash(const std::string& filename);

  std::map<std::pair<boost::gregorian::date,std::string>,DailyCash> getMapOfDailyCash();

  void printMapOfDailyCash();

  std::set<boost::gregorian::date> getSetOfUniqueDates();

/** @brief To String  */
std::string toString() const;

 private:

  boost::gregorian::date m_ref_date;
  std::string m_bond_code;
  std::string m_bond_type;
  std::string m_market_code;
  double m_ref_price;
  double m_mid_yield;
  double m_max_price;
  double m_min_price;
  double m_wgt_price;
  double m_accrued_int;
  boost::gregorian::date m_settl_date;
  int m_part_number;
  int m_num_oblg;
  double m_total_volume;
  double m_avg_size;
  double m_avg_spread;
  int m_imb_trade;
  std::map<std::pair<boost::gregorian::date,std::string>,DailyCash> m_map_dailycash;

};
#endif // DAILYCASH_H_
