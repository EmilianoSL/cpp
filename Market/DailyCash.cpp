#include <sstream>
#include <ostream>
#include "DailyCash.h"
#include <boost/lexical_cast.hpp>

/** @brief Constructor */

DailyCash::DailyCash(
	    const boost::gregorian::date& ref_date,
	    const std::string& bond_code,
	    const std::string& bond_type,
	    const std::string& market_code,
	    const double ref_price,
	    const double mid_yield,
	    const double max_price,
	    const double min_price,
	    const double wgt_price,
	    const double accrued_int,
	    const boost::gregorian::date& settl_date,
	    const int part_number,
	    const int num_oblg,
	    const double total_volume,
	    const double avg_size,
	    const double avg_spread,
	    const int imb_trade) :

  m_ref_date(ref_date),
  m_bond_code(bond_code),
  m_bond_type(bond_type),
  m_market_code(market_code),
  m_ref_price(ref_price),
  m_mid_yield(mid_yield),
  m_max_price(max_price),
   m_min_price(min_price),
   m_wgt_price(wgt_price),
   m_accrued_int(accrued_int),
  m_settl_date(settl_date),
  m_part_number(part_number),
  m_num_oblg(num_oblg),
  m_total_volume(total_volume),
  m_avg_size(avg_size),
  m_avg_spread(avg_spread),
   m_imb_trade(imb_trade)
{}


DailyCash::DailyCash(const std::string& dummy_string):
  m_bond_code(dummy_string){};
/** @brief Destructor */
DailyCash::~DailyCash(){}

/** @brief Getter methods */

boost::gregorian::date DailyCash::getRefDate() const{
  return m_ref_date;
}
std::string DailyCash::getBondCode() const {
  return m_bond_code;
}
std::string DailyCash::getBondType() const {
  return m_bond_type;
}
std::string DailyCash::getMarketCode() const {
  return m_market_code;
}
double DailyCash::getRefPrice() const {
  return m_ref_price;
}
double DailyCash::getMidYield() const {
  return m_mid_yield;
}
double DailyCash::getMaxPrice() const {
  return m_max_price;
}
double DailyCash::getMinPrice() const {
  return m_min_price;
}
double DailyCash::getWgtPrice() const {
  return m_wgt_price;
}
double DailyCash::getAccruedInt() const {
  return m_accrued_int;
}
boost::gregorian::date DailyCash::getSettlDate() const {
  return m_settl_date;
}
int DailyCash::getPartNumber() const {
  return m_part_number;
}
int DailyCash::getNumOblg() const {
  return m_num_oblg;
}
double DailyCash::getTotalVolume() const {
  return m_total_volume;
}
double DailyCash::getAvgSize() const {
  return m_avg_size;
}
double DailyCash::getAvgSpread() const {
  return m_avg_spread;
}
int DailyCash::getImbTrade() const {
 return m_imb_trade;
}



void DailyCash::makeMapOfDailyCash(const std::string& filename){
  
  io::CSVReader<17> in(filename);

in.read_header(io::ignore_extra_column,
  		 "RefDate",
  		 "BondCode",
  		 "BondType",
  		 "MarketCode",
  		 "RefPrice",
  		 "MidYield",
  		 "MaxPrice",
  		 "MinPrice",
  		 "WgtPrice",
  		 "AccruedInt",
  		 "SettlDate",
  		 "PartNumber",
  		 "NumOblg",
  		 "TotalVolume",
  		 "AvgSize",
  		 "AvgSpread",
  		 "ImbTrade");

  std::string RefDate;
  std::string BondCode;
  std::string BondType;
  std::string MarketCode;
  double RefPrice;
  double MidYield;
  double MaxPrice;
  double MinPrice;
  double WgtPrice;
  double AccruedInt;
  std::string SettlDate;
  int PartNumber;
  int NumOblg;
  int TotalVolume;
  int AvgSize;
  double AvgSpread;
  int ImbTrade;

  std::map<std::pair<boost::gregorian::date,std::string>,DailyCash> map_cash;
  
  while (in.read_row(RefDate,
  		     BondCode,
  		     BondType,
  		     MarketCode,
  		     RefPrice,
  		     MidYield,
  		     MaxPrice,
  		     MinPrice,
  		     WgtPrice,
  		     AccruedInt,
  		     SettlDate,
  		     PartNumber,
  		     NumOblg,
  		     TotalVolume,
  		     AvgSize,
  		     AvgSpread,
  		     ImbTrade)
  	 ){


    boost::gregorian::date ref_date = boost::gregorian::from_simple_string(RefDate);
    boost::gregorian::date settl_date = boost::gregorian::from_simple_string(SettlDate);
    
    DailyCash Daily_Cash(ref_date,
  			 BondCode,
  			 BondType,
  			 MarketCode,
  			 RefPrice,
  			 MidYield,
  			 MaxPrice,
  			 MinPrice,
  			 WgtPrice,
  			 AccruedInt,
  			 settl_date,
  			 PartNumber,
  			 NumOblg,
  			 TotalVolume,
  			 AvgSize,
  			 AvgSpread,
  			 ImbTrade);
    

    std::pair<boost::gregorian::date,std::string>
      key = std::make_pair(Daily_Cash.getRefDate(),Daily_Cash.getBondCode());

    map_cash.insert(std::pair<std::pair<boost::gregorian::date,std::string>,DailyCash>(key,Daily_Cash));

    m_map_dailycash = map_cash;

  }}

  std::map<std::pair<boost::gregorian::date,std::string>,DailyCash> 
DailyCash::getMapOfDailyCash(){
    return m_map_dailycash;
  }

  void DailyCash::printMapOfDailyCash(){
    std::map<std::pair<boost::gregorian::date,std::string>,DailyCash> map_cash = 
      getMapOfDailyCash();
    for (auto& element : map_cash) {
      std::cout << to_iso_extended_string(element.first.first)
  	        << "  - "
  		<< element.first.second  << std::endl;
    }
  }


std::set<boost::gregorian::date> DailyCash::getSetOfUniqueDates(){
   std::map<std::pair<boost::gregorian::date,std::string>,DailyCash> map_cash = 
      getMapOfDailyCash();
   std::vector<boost::gregorian::date> dates_vector;
   for (auto& element : map_cash) {
     dates_vector.push_back(element.first.first);
   }

   std::sort(dates_vector.begin(),dates_vector.end());
  dates_vector.erase(std::unique(dates_vector.begin(),dates_vector.end()),dates_vector.end());
  std::set<boost::gregorian::date> dates_set(dates_vector.begin(),dates_vector.end());

  return dates_set;
}

/** @brief To String  */

std::string DailyCash::toString() const {
  std::ostringstream out;
  out <<  "\r\n" "RefDate: ";
  out << to_iso_extended_string(m_ref_date) << "\r\n";
  out << "BondCode: ";
  out << m_bond_code << "\r\n";
  out << "BondType: ";
  out << m_bond_type << "\r\n";
  out << "MarketCode: ";
  out << m_market_code << "\r\n";
  out << "RefPrice: ";
  out << m_ref_price << "\r\n";
  out << "MidYield: ";
  out << m_mid_yield << "\r\n";
  out << "MaxPrice: ";
  out << m_max_price << "\r\n";
  out << "MinPrice: ";
  out << m_min_price << "\r\n";
  out << "WgtPrice: ";
  out << m_wgt_price << "\r\n";
  out << "AccruedInt: ";
  out << m_accrued_int << "\r\n";
  out << "SettlDate: ";
  out << to_iso_extended_string(m_settl_date) << "\r\n";
  out << "PartNumber: ";
  out << m_part_number << "\r\n";
  out << "NumOblg: ";
  out << m_num_oblg << "\r\n";
  out << "TotalVolume: ";
  out << m_total_volume << "\r\n";
  out << "AvgSize: ";
  out << m_avg_size << "\r\n";
  out << "AvgSpread: ";
  out << m_avg_spread << "\r\n";
  out << "ImbTrade: ";
  out << m_imb_trade << "\r\n";
  
  return out.str();
}
