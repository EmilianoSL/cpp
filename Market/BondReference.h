#ifndef BONDREFERENCE_H_
#define BONDREFERENCE_H_


#include <string>
#include <map>
#include <set>

#include "../helpers/csv.h"

#define BOOST_DATE_TIME_NO_LIB
#include <boost/date_time.hpp>

class BondReference{
 public:
/** @brief Constructor */
  BondReference(
		const std::string& bond_code,
		const std::string& bond_type,
		const std::string& description,
		const std::string& market_code,
		const std::string& country,
		const std::string& currency,
		const std::string& day_count,
		const std::string& coupon_freq,
		const std::string& coupon_type,
		const double coupon_rate,
		const boost::gregorian::date&  issue_date,
		const boost::gregorian::date&  maturity_date,
		const double lot_size,
		const boost::gregorian::date& first_settl_date,
		const boost::gregorian::date& last_coupon_date);

  BondReference(const std::string& dummy_string);

/** @brief Destructor */
    virtual ~BondReference();
/** @brief Getter methods */
    std::string getBondCode() const;
    std::string getBondType() const;
    std::string getDescription() const;
    std::string getMarketCode() const;
    std::string getCountry() const;
    std::string getCurrency() const;
    std::string getDayCount() const;
    std::string getCouponFreq() const;
    std::string getCouponType() const;
    double getCouponRate() const;
    boost::gregorian::date getIssueDate() const;
    boost::gregorian::date getMaturityDate() const;
    double getLotSize() const;
    boost::gregorian::date getFirstSettlDate() const;
    boost::gregorian::date getLastCouponDate() const;

    std::set<std::string> getSetOfUniqueCountries();
    
/** @brief To String  */
std::string toString() const;

 int getCouponFreqAsInteger(const std::string& coupon_freq);

 void makeMapOfBonds(const std::string& filename);

 std::map<std::string,BondReference> getMapOfBonds();

 void printMapOfBonds();

 private:

std::string m_bond_code;
std::string m_bond_type;
std::string m_description;
std::string m_market_code;
std::string m_country;
std::string m_currency;
std::string m_day_count;
std::string m_coupon_freq;
std::string m_coupon_type;
double m_coupon_rate;
boost::gregorian::date m_issue_date;
boost::gregorian::date  m_maturity_date;
double m_lot_size;
boost::gregorian::date m_first_settl_date;
boost::gregorian::date m_last_coupon_date;
std::map<std::string,BondReference> m_map_of_bonds;

};

#endif // BONDREFERENCE_H_
