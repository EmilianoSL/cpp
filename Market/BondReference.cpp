#include <sstream>
#include <ostream>
#include "BondReference.h"
#include <boost/lexical_cast.hpp>

/** @brief Constructor */
BondReference::BondReference(
			     const std::string& bond_code,
			     const std::string& bond_type,
			     const std::string& description,
			     const std::string& market_code,
			     const std::string& country,
			     const std::string& currency,
			     const std::string& day_count,
			     const std::string& coupon_freq,
			     const std::string& coupon_type,
			     const double coupon_rate,
			     const boost::gregorian::date& issue_date,
			     const boost::gregorian::date&  maturity_date,
			     const double lot_size,
			     const boost::gregorian::date& first_settl_date,
			     const boost::gregorian::date& last_coupon_date):

  m_bond_code(bond_code),
  m_bond_type(bond_type),
  m_description(description),
  m_market_code(market_code),
  m_country(country),
  m_currency(currency),
  m_day_count(day_count),
  m_coupon_freq(coupon_freq),
  m_coupon_type(coupon_type),
  m_coupon_rate(coupon_rate),
  m_issue_date(issue_date),
  m_maturity_date(maturity_date),
  m_lot_size(lot_size),
  m_first_settl_date(first_settl_date),
  m_last_coupon_date(last_coupon_date)
{}

BondReference::BondReference(const std::string& dummy_string):
  m_description(dummy_string){};

/** @brief Destructor */
BondReference::~BondReference(){}

/** @brief Getter methods */


std::string BondReference::getBondCode() const {
  return m_bond_code;
}

std::string BondReference::getBondType() const {
  return m_bond_type;
}

std::string BondReference::getDescription() const {
  return m_description;
}

std::string BondReference::getMarketCode() const {
  return m_market_code;
}

std::string BondReference::getCountry() const {
  return m_country;
}

std::string BondReference::getCurrency() const {
  return m_currency;
}

std::string BondReference::getDayCount() const {
  return m_day_count;
}

std::string BondReference::getCouponFreq() const {
  return m_coupon_freq;
}

std::string BondReference::getCouponType() const {
  return m_coupon_type;
}

double BondReference::getCouponRate() const {
  return m_coupon_rate;
}

boost::gregorian::date BondReference::getIssueDate() const {
  return m_issue_date;
}

boost::gregorian::date BondReference::getMaturityDate() const {
  return m_maturity_date;
}

double BondReference::getLotSize() const {
  return m_lot_size;
}

boost::gregorian::date BondReference::getFirstSettlDate() const {
  return m_first_settl_date;
}

boost::gregorian::date BondReference::getLastCouponDate() const {
  return m_last_coupon_date;
}


int BondReference::getCouponFreqAsInteger(const std::string& coupon_freq) {

  if(coupon_freq == "ANNUALLY"){ return 1;}
  else if ( coupon_freq == "SEMIANUALLY"){ return 6;}
  else if ( coupon_freq == "QUARTERLY"){ return 3;}
  else if ( coupon_freq == "ZERO") { return 0;}
  else return 0;
}


void BondReference::makeMapOfBonds(const std::string& filename){

  const int numberOfColumns = 15;
  io::CSVReader<numberOfColumns> in(filename);

  in.read_header(io::ignore_extra_column,
  		 "BondCode",
  		 "BondType",
  		 "Description",
  		 "MarketCode",
  		 "Country",
  		 "Currency",
  		 "DayCount",
  		 "CouponFreq",
  		 "CouponType",
  		 "CouponRate",
  		 "IssueDate",
  		 "MaturityDate",
  		 "LotSize",
  		 "FirstSettlDate",
  		 "LastCouponDate");

  std::string BondCode;
  std::string BondType;
  std::string MarketCode;
  std::string Description;
  std::string Country;
  std::string Currency;
  std::string DayCount;
  std::string CouponFreq;
  std::string CouponType;
  double  CouponRate;
  std::string IssueDate;
  std::string MaturityDate;
  double LotSize;
  std::string FirstSettlDate;
  std::string LastCouponDate;
  
  std::map<std::string,BondReference> map_bonds;
  int i = 0;
  while (in.read_row(BondCode,
  		     BondType,
  		     Description,
  		     MarketCode,
  		     Country,
  		     Currency,
  		     DayCount,
  		     CouponFreq,
  		     CouponType,
  		     CouponRate,
  		     IssueDate,
  		     MaturityDate,
  		     LotSize,
  		     FirstSettlDate,
  		     LastCouponDate)
  	 ){

    boost::gregorian::date issue_date, mat_date, first_settl_date, last_coupon_date;
 
    issue_date = boost::gregorian::from_simple_string(IssueDate);
    mat_date = boost::gregorian::from_simple_string(MaturityDate);
    first_settl_date = boost::gregorian::from_simple_string(FirstSettlDate);
    last_coupon_date = boost::gregorian::from_simple_string(LastCouponDate);

     BondReference bond_reference(BondCode,
  				 BondType,
  				 Description,
  				 MarketCode,
  				 Country,
  				 Currency,
  				 DayCount,
  				 CouponFreq,
  				 CouponType,
  				 CouponRate,
  				 issue_date,
  				 mat_date,
  				 LotSize,
  				 first_settl_date,
  				 last_coupon_date);

     map_bonds.insert(std::pair<std::string,BondReference>(bond_reference.getBondCode(),bond_reference));
     m_map_of_bonds = map_bonds;
  }
}

std::map<std::string,BondReference> BondReference::getMapOfBonds(){
  return m_map_of_bonds;
}

void BondReference::printMapOfBonds(){
  std::map<std::string,BondReference> map_bonds = getMapOfBonds();
  std::map<std::string,BondReference>::iterator it;
  for (it = map_bonds.begin(); it!= map_bonds.end(); it++){
    std::cout << it->first << " - " << it->second.getDescription() << std::endl;
  }
}

std::set<std::string> BondReference::getSetOfUniqueCountries(){
  
  auto map_bonds = getMapOfBonds();

  std::vector<std::string> countries_vector;

  for (auto& element : map_bonds){
    countries_vector.push_back(element.second.getCountry());
  }
			       
  std::sort(countries_vector.begin(),countries_vector.end());
  countries_vector.erase(
			 std::unique(
				     countries_vector.begin(),
				     countries_vector.end()
				     ),
			 countries_vector.end()
			 );

  std::set<std::string> countries_set(countries_vector.begin(),countries_vector.end());

    return countries_set;
  
}

/** @brief To String  */

std::string BondReference::toString() const {
  std::ostringstream out;
  out << "\r\n" "BondCode: ";
  out << m_bond_code << "\r\n";
  out << "BondType: ";
  out << m_bond_type << "\r\n";
  out << "Description: ";
  out << m_description << "\r\n";
  out << "MarketCode: ";
  out << m_market_code << "\r\n";
  out << "Country: ";
  out << m_country << "\r\n";
  out << "Currency: ";
  out << m_currency << "\r\n";
  out << "DayCount: ";
  out << m_day_count << "\r\n";
  out << "CouponFreq: ";
  out << m_coupon_freq << "\r\n";
  out << "CouponType: ";
  out << m_coupon_type << "\r\n";
  out << "CouponRate: ";
  out << m_coupon_rate << "\r\n";
  out << "IssueDate: ";
  out << to_iso_extended_string(m_issue_date) << "\r\n";
  out << "MaturityDate: ";
  out << to_iso_extended_string(m_maturity_date) << "\r\n";
  out << "LotSize: ";
  out << m_lot_size << "\r\n";
  out << "FirstSettlDate: ";
  out << to_iso_extended_string(m_first_settl_date) << "\r\n";
  out << "LastCouponDate: ";
  out << to_iso_extended_string(m_last_coupon_date) << "\r\n";
  
  return out.str();

}
