#include "conventionshelper.h"

namespace conventionshelper {

/**@brief Calculate days bewteen start_date (excl.) and end_date (incl.) */
int calculateDaysBetweenForActual(
    const boost::gregorian::date& start_date,
    const boost::gregorian::date& end_date){
  return (end_date - start_date).days();
}

  int calculateDaysBetween(const boost::gregorian::date& start_date,
			    const boost::gregorian::date& end_date,
			   const conventionshelper::DayCountConvention& day_count_convention){
    switch (day_count_convention){
    case ACTUAL_360:
    case ACTUAL_365:
      return calculateDaysBetweenForActual(start_date,end_date);
    default:
      return calculateDaysBetweenForActual(start_date,end_date);
    }

}


/**@brief Calculate Year Fraction  bewteen start_date (excl.) and end_date (incl.) */

double calculateYearFractionActual365(
       const boost::gregorian::date& start_date,
       const boost::gregorian::date& end_date){
  return calculateDaysBetweenForActual(start_date,end_date) / 365.0;
}

double calculateYearFractionActual360(
       const boost::gregorian::date& start_date,
       const boost::gregorian::date& end_date){
   return calculateDaysBetweenForActual(start_date,end_date) / 360.0;
}

double calculateYearFractionActualActual(
       const boost::gregorian::date& start_date,
       const boost::gregorian::date& end_date){

  long int days_leap_year = 0;
  long int days_normal_year = 0;
  long int total_days = (end_date - start_date).days();

  for (int i=1 ; i <= total_days ; i++){
    auto date = start_date + boost::gregorian::days(i);
    bool is_leap = boost::gregorian::gregorian_calendar::is_leap_year(date.year());
    if(is_leap)  days_leap_year   +=1;
    if(!is_leap) days_normal_year +=1;
  }

  double year_fraction = days_leap_year/366.0 + days_normal_year/365.0;

  return year_fraction;
}

  double calculateYearFraction(const boost::gregorian::date& start_date,
			       const boost::gregorian::date& end_date,
			       const conventionshelper::DayCountConvention& day_count_convention){
    switch (day_count_convention){
    case ACTUAL_360:
      return calculateYearFractionActual360(start_date,end_date);
    case ACTUAL_365:
      return calculateYearFractionActual365(start_date,end_date);
    case ACTUAL_ACTUAL:
      return calculateYearFractionActualActual(start_date,end_date);
    default:
      return calculateYearFractionActual365(start_date,end_date);
    }

}

DayCountConvention 
getStrDayCountConventionAsEnum(const std::string& day_count_convention)  {

  if (day_count_convention == "ACTUAL_360"){
    return conventionshelper::ACTUAL_360;}
  else if (day_count_convention == "ACTUAL_365"){
      return conventionshelper::ACTUAL_365;}
  else return conventionshelper::ACTUAL_360;
};


} // namespace conventionshelper


