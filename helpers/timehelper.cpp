#include "timehelper.h"

#include <algorithm>

namespace timehelper {

  
std::vector<boost::gregorian::date> generateDates(const boost::gregorian::date& valuation_date,
						  int tenor,
						  const std::string& time_unit,
						  int frequency){
  
  std::vector<boost::gregorian::date> dates_vector;
  boost::gregorian::date end_date = timehelper::addToDate(valuation_date,tenor,time_unit);
  
  boost::gregorian::date date = valuation_date;
  do {
    date = timehelper::addToDate(date,1,time_unit);
    dates_vector.push_back(date);
  } while (date < end_date);
  return dates_vector;							 
}

std::vector<boost::gregorian::date> generateDates(const boost::gregorian::date& valuation_date,
						  int tenor,
						  const timehelper::TimeUnit& time_unit,
						  int frequency){
  
  std::vector<boost::gregorian::date> dates_vector;
  boost::gregorian::date end_date = timehelper::addToDate(valuation_date,tenor,time_unit);
  
  boost::gregorian::date date = valuation_date;
  do {
    date = timehelper::addToDate(date,1,time_unit);
    dates_vector.push_back(date);
  } while (date < end_date);
  return dates_vector;							 
}


  std::vector<boost::gregorian::date> 
   generateDatesBackward(const boost::gregorian::date& valuation_date,
  			 const boost::gregorian::date& maturity_date,
  			 const boost::gregorian::date& last_coupon_date,
  			 const int& frequency,
			 const timehelper::TimeUnit& time_unit){
  
  std::vector<boost::gregorian::date> dates_vector;
  
  if (valuation_date > maturity_date) { return dates_vector; }
  
  dates_vector.push_back(maturity_date);

  if (valuation_date > last_coupon_date) { return dates_vector; }

  if (frequency == 0 ) { return dates_vector; }
 
  double time_to_add;

time_to_add = static_cast<double>(frequency * -1 );
  
  boost::gregorian::date new_date;
  new_date = last_coupon_date;
  
  do {
       dates_vector.push_back(new_date);     
       new_date = timehelper::addToDate(new_date,time_to_add,time_unit);
     
} while (valuation_date < new_date);

  sort(dates_vector.begin(),dates_vector.end());

  return dates_vector;
  
}


 boost::gregorian::date addToDate(const boost::gregorian::date& start_date,
				  double time_to_add,
				  const std::string& time_unit){
      if (time_unit == "DAY"){
	return start_date + boost::gregorian::days(time_to_add);}
      else if (time_unit == "WEEK"){
	return start_date + boost::gregorian::weeks(time_to_add);}
      else if (time_unit == "MONTH"){
	return start_date + boost::gregorian::months(time_to_add);}
      else if (time_unit == "YEAR"){
	return start_date + boost::gregorian::years(time_to_add);}
      else {
	return start_date;}
 }


 boost::gregorian::date addToDate(const boost::gregorian::date& start_date,
				  double time_to_add,
				  const timehelper::TimeUnit& time_unit){
   switch(time_unit){
   case DAY:
     return start_date + boost::gregorian::days(time_to_add);
   case WEEK:
     return start_date + boost::gregorian::weeks(time_to_add);
   case MONTH:
     return start_date + boost::gregorian::months(time_to_add);
   case YEAR:
     return start_date + boost::gregorian::years(time_to_add);
   default:
     return start_date;
   }
 }
     

  timehelper::TimeUnit getTimeUnitAsEnum(const std::string& time_unit){

    if (time_unit == "DAY"){
      return timehelper::DAY;
    }
    else  if (time_unit == "WEEK"){
      return timehelper::WEEK;
    }
    else  if (time_unit == "MONTH"){
      return timehelper::MONTH;
    }
   else  if (time_unit == "YEAR"){
     return timehelper::YEAR;
   }
   else {
      return timehelper::YEAR;
   }
  }


  timehelper::Frequency getFrequencyAsEnum(const std::string& frequency) {

    if (frequency == "NOFREQUENCY"){
      return timehelper::NOFREQUENCY;}
    else if(frequency == "ONCE"){
      return timehelper::ONCE;
    }
    else if(frequency == "MONTHLY"){
      return timehelper::MONTHLY;
    }
    else if(frequency == "BIMONTHLY"){
      return timehelper::BIMONTHLY ;
    }
    else if(frequency == "QUARTERLY"){
      return timehelper::QUARTERLY;
    }
    else if(frequency == "EVERYFOURMONTH"){
      return timehelper::EVERYFOURMONTH ;
    }
    else if(frequency == "SEMIANUALLY"){
      return timehelper::SEMIANUALLY;
    }
    else if(frequency == "ANUALLY"){
      return timehelper::ANNUALLY;
    }
    else if(frequency == "EVERYFOURTHWEEK"){
      return timehelper::EVERYFOURTHWEEK;
    }
    else if(frequency == "BIWEEKLY"){
      return timehelper::BIWEEKLY;
    }
    else if(frequency == "WEEKLY"){
      return timehelper::WEEKLY;
    }
    else if(frequency == "DAILY"){
      return timehelper::DAILY;
    }
    else if(frequency == "OTHER"){
      return timehelper::OTHER ;
    }
    else {
     return timehelper::NOFREQUENCY;
    }
        
  }

 int getCouponFreqAsInteger(const std::string& coupon_freq) {

  if(coupon_freq == "ANNUALLY"){ return 1;}
  else if ( coupon_freq == "SEMIANUALLY"){ return 6;}
  else if ( coupon_freq == "QUARTERLY"){ return 3;}
  else if ( coupon_freq == "ZERO") { return 0;}
  else return 0;
 }


  timehelper::TimeUnit getTimeUnitFromFrequency(const std::string& frequency) {
    
    timehelper::Frequency freq = getFrequencyAsEnum(frequency);

    switch (freq) {
    case DAILY:
      return timehelper::DAY;
    case WEEKLY: case BIWEEKLY: 
      return timehelper::WEEK;
    case MONTHLY: case BIMONTHLY: case QUARTERLY: case EVERYFOURMONTH: case  SEMIANUALLY:
      return timehelper::MONTH;
    case ANNUALLY:
      return timehelper::YEAR;
    default:
      return timehelper::YEAR;
    }
  }

 

} //namespace TIMEHELPER_H_

