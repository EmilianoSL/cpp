#ifndef CONVENTIONSHELPER_H_
#define CONVENTIONSHELPER_H_

#define BOOST_DATE_TIME_NO_LIB
#include <boost/date_time.hpp>

#include <string>

namespace conventionshelper {

enum DayCountConvention
{
  ACTUAL_365,
  ACTUAL_360,
  ACTUAL_ACTUAL
};

enum BusinessDayConvention
{
  following = 1,
  mod_following = 2,
  preeceding = 3, 
  mod_preceeding = 4
};

/**@brief Convert from std::string to enum  */
 DayCountConvention getStrDayCountConventionAsEnum(
    const std::string& day_count_convention) ;

BusinessDayConvention getStringBusinessDayConventionAsEnum(
    const std::string& business_day_convention);

/**@brief Calculate days bewteen start_date (excl.) and end_date (incl.) */
int calculateDaysBetweenForActual(
    const boost::gregorian::date& start_date,
    const boost::gregorian::date& end_date);

int calculateDaysBetween(
    const boost::gregorian::date& start_date,
    const boost::gregorian::date& end_date,
    const DayCountConvention& day_count_convention);


double calculateYearFractionActual360(
       const boost::gregorian::date& start_date,
       const boost::gregorian::date& end_date);

 double calculateYearFractionActualActual(
       const boost::gregorian::date& start_date,
       const boost::gregorian::date& end_date);

double calculateYearFraction(
       const boost::gregorian::date& start_date,
       const boost::gregorian::date& end_date,
       const DayCountConvention& day_count_convention);
 
} //namespace conventionshelper
#endif // CONVENTIONSHELPER_H_
