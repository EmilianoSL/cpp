#include <vector>
#include <tuple>
#include <memory>
#include <map>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <ctime>
#include "math.h"
#include <chrono>

#include "timehelper.h"
#include "conventionshelper.h"
#include "matrixhelper.h"
#include "regressionhelper.h"
#include "InterfaceCLAPACK.h"
#include "Grid.h"
#include "csv.h"
#include "../Portfolio/Bucket.h"
#include "../Portfolio/OneCashFlowFix.h"
#include "../Portfolio/Leg.h"
#include "../Portfolio/Bond.h"
#include "../Portfolio/Portfolio.h"
#include "../Portfolio/Instrument.h"
#include "../TermStructure/TermStructure.h"
#include "../TermStructure/TermStructureFlat.h"
#include "../TermStructure/TermStructureParametric.h"
#include "../Market/BondReference.h"
#include "../Market/DailyCash.h"

#include <blitz/array.h>
#include <boost/make_shared.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>
#define BOOST_DATE_TIME_NO_LIB
#include <boost/date_time.hpp>

double objfun(Portfolio& portfolio,
	      const blitz::Array<double,2>& C,
	      const blitz::Array<double,2>& W,
	      const blitz::Array<double,1>& T,
	      const blitz::Array<double,1>& P,
	      const TermStructure& term_structure);

double objfun(Portfolio& portfolio,
	      const blitz::Array<double,2>& x,
	      const blitz::Array<double,2>& C,
	      const blitz::Array<double,2>& W,
	      const blitz::Array<double,1>& T,
	      const blitz::Array<double,1>& P,
	      const boost::gregorian::date& start_date,
	      const boost::gregorian::date& end_date,
	      const CurveType& curve_type,
	      const ParametricForm& param_form,
	      const std::string& day_count
	      );

double objfun(
	      double x[6],
	      Portfolio& portfolio,
	      const blitz::Array<double,2>& C,
	      const blitz::Array<double,2>& W,
	      const blitz::Array<double,1>& T,
	      const blitz::Array<double,1>& P,
	      const boost::gregorian::date& start_date,
	      const boost::gregorian::date& end_date,
	      const CurveType& curve_type,
	      const ParametricForm& param_form,
	      const std::string& day_count
	      );

double objfuncon(
	      double x[6],
	      Portfolio& portfolio,
	      const blitz::Array<double,2>& C,
	      const blitz::Array<double,2>& W,
	      const blitz::Array<double,1>& T,
	      const blitz::Array<double,1>& P,
	      const boost::gregorian::date& start_date,
	      const boost::gregorian::date& end_date,
	      const CurveType& curve_type,
	      const ParametricForm& param_form,
	      const std::string& day_count
	      );

double penalty(const blitz::Array<double,2>& x, double& x1_min);


						      
