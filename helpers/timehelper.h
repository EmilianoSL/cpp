#ifndef TIMEHELPER_H_
#define TIMEHELPER_H_

#include <string>
#include <vector>
#define BOOST_DATE_TIME_NO_LIB
#include <boost/date_time.hpp>

namespace timehelper {

enum Frequency {
  NOFREQUENCY = -1,
  ONCE = 0,
  MONTHLY = 1,
  BIMONTHLY = 2,
  QUARTERLY = 3,
  EVERYFOURMONTH = 4,
  SEMIANUALLY = 6,
  ANNUALLY = 12,
  EVERYFOURTHWEEK = 13,
  BIWEEKLY = 26,
  WEEKLY = 52,
  DAILY = 365,
  OTHER = 999
};

enum TimeUnit {
      DAY,
      WEEK,
      MONTH,
      YEAR
};

enum DateRule {
  FORWARD,
  BACKWARD,
  ZERO
  };

enum Weekday {
   SUNDAY    = 1,
   MONDAY    = 2,
   TUESDAY   = 3,
   WEDNESDAY = 4,
   THURSDAY  = 5,
   FRIDAY    = 6,
   SATURDAY  = 7,
   SUN = 1,
   MON = 2,
   TUE = 3,
   WED = 4,
   THU = 5,
   FRI = 6,
   SAT = 7
};


 std::vector<boost::gregorian::date> generateDates(const boost::gregorian::date& valuation_date,
						   int tenor,
						   const std::string& time_unit,
						   int frequency);

 std::vector<boost::gregorian::date> generateDates(const boost::gregorian::date& valuation_date,
						  int tenor,
						  const timehelper::TimeUnit& time_unit,
						  int frequency);
 						  
 std::vector<boost::gregorian::date> generateDatesBackward(
 			 const boost::gregorian::date& valuation_date,
 			 const boost::gregorian::date& maturity_date,
 			 const boost::gregorian::date& last_coupon_date,
 			 const int& frequency,
			 const timehelper::TimeUnit& time_unit);
    

boost::gregorian::date addToDate(const boost::gregorian::date& start_date,
				 double time_to_add,
				 const std::string& time_unit);

boost::gregorian::date addToDate(const boost::gregorian::date& start_date,
				 double time_to_add,
				 const timehelper::TimeUnit& time_unit);

 timehelper::Frequency getFrequencyAsEnum(const std::string& frequency);

 timehelper::TimeUnit getTimeUnitAsEnum(const std::string& time_unit);

 timehelper::TimeUnit getTimeUnitFromFrequency(const std::string& frequency);

 int getCouponFreqAsInteger(const std::string& coupon_freq);


} // namespace timehelper
#endif // TIMEHELPER_H_
