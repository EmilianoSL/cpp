#include <map>
#include <algorithm>
#include "Grid.h"


Grid::Grid(const int& level,const std::vector<std::pair<double,double> > domain) :
  m_level(level),
  m_domain(domain)
{
  m_delta = pow(2,-1 * getLevel());
  setCanonicalVector();
  setIndexVector();
  setCanonicalNodes();
  m_number_of_nodes = getCanonicalNodes().size();

}
  
 Grid::~Grid(){}

int Grid::getLevel() const {
  return m_level;
}

double Grid::getDelta() const {
  return m_delta;
}

void Grid::setCanonicalVector() {
  const double delta = getDelta();
  int num_points = (0.5 / delta ) + 1;
  std::vector<double> v;
  v.push_back(0);
  for (int i = 1 ; i < num_points; i++){
    v.push_back(v[i-1] + delta) ;
  }
  m_canonical_vec = v;
}

void Grid::setIndexVector() {
  int level = getLevel();
  int k = level - 2;
  std::vector<std::pair<int,int> > index_vec;
  auto p1 = std::make_pair(level,1);
  auto p2 = std::make_pair(1,level);

  index_vec.push_back(p1);
  index_vec.push_back(p2);
  
  std::vector<int> first;
  std::vector<int> second;
  
  for (int i = k ; i >= 2 ; i--){first.push_back(i);}
  for (int i = 2 ; i <= k ; i++){second.push_back(i);}

  for (int i = 0 ; i < second.size() ; i++){
    auto pair = std::make_pair(first[i],second[i]);
    index_vec.push_back(pair);
  }
  
  m_index_vec = index_vec;
}



std::vector<double> Grid::getCanonicalVector() const {
  return m_canonical_vec;
}

std::vector<std::pair<double,double> > Grid::getCanonicalNodes() const {
  return m_canonical_nodes;
}

std::vector<std::pair<int,int> > Grid::getIndexVector() const {
  return m_index_vec;
}

std::vector<std::pair<double,double> > Grid::getDomain() const {
  return m_domain;
}

blitz::Array<double,1> Grid::makeCanonicalArray() {

  const std::vector<double> canonical_vec = getCanonicalVector();
  blitz::Array<double,1> V(canonical_vec.size());
  for (int i = 0 ; i < canonical_vec.size() ; i++){
    V(i) = canonical_vec[i];
  }
  return V;
}


void Grid::setCanonicalNodes() {

  auto index_vec = getIndexVector();
  std::map<int,std::vector<double> > canonical_map;
  for (int i = 0 ; i < index_vec.size() ; i++){
    std::vector<double> vec;
    int level = index_vec[i].first;
    double delta = pow(2, -1 * level);
    int num_points = (0.5 / delta ) + 1;
    vec.push_back(0);
    for (int j = 1 ; j < num_points ; j++){
      vec.push_back(vec[j-1] + delta);
    }
    canonical_map.insert(std::pair<int,std::vector<double> >(level,vec) );
  }

   std::vector<std::pair<double,double> > T;

   for (int k = 0 ; k < index_vec.size() ; k++){
     std::map<int,std::vector<double> >::iterator it1;
     std::map<int,std::vector<double> >::iterator it2;
     it1 = canonical_map.find(index_vec[k].first);
     it2 = canonical_map.find(index_vec[k].second);
     std::vector<double> vec1 = it1->second;
     std::vector<double> vec2 = it2->second;
     int a = vec1.size();
     int b = vec2.size();
     std::vector<std::vector<double> > w;
     std::vector<std::vector<double> > v;
     for (int i=0;i<b;i++){w.push_back(vec1);}
     for (int i=0;i<a;i++){v.push_back(vec2);}
     std::vector<double> w_col;
     std::vector<double> v_col;

     for (int j = 0 ; j < a ; j++){
       for (int i = 0 ; i <  w.size() ; i++){
	 w_col.push_back(w[i][j]);
       }
     }

     for (int j = 0 ; j < v.size() ; j++){
       for (int i = 0 ; i <  b ; i++){
	 v_col.push_back(v[j][i]);
       }
     }

     long int n = w.size() * v.size();
     // [0.0 x 0.5]
     for(int i = 0 ; i < n ; i++){
       auto pair = std::make_pair(w_col[i],v_col[i]);
       T.push_back(pair);
     }

     for(int i = 0 ; i < n ; i++){
       auto pair = std::make_pair(-1*w_col[i],-1*v_col[i]);
       T.push_back(pair);
     }

     for(int i = 0 ; i < n ; i++){
       auto pair = std::make_pair(-1*w_col[i],v_col[i]);
       T.push_back(pair);
     }

     for(int i = 0 ; i < n ; i++){
       auto pair = std::make_pair(w_col[i],-1*v_col[i]);
       T.push_back(pair);
     }

     std::sort(T.begin(),T.end());
     T.erase(std::unique(T.begin(),T.end()),T.end());
   }

   // Eliminate Boundaries and Inner Cross

   for (int i=0 ; i < T.size() ; i++){
     auto pair = T[i];
     if (std::get<0>(pair) == 0.5) T.erase(T.begin()+i);
     if (std::get<0>(pair) == -0.5) T.erase(T.begin()+i);
     if (std::get<1>(pair) == 0.5) T.erase(T.begin()+i);
     if (std::get<1>(pair) == -0.5) T.erase(T.begin()+i);
     if (std::get<0>(pair) == 0) T.erase(T.begin()+i);
     if (std::get<1>(pair) == 0) T.erase(T.begin()+i);
      
   }
   
   m_canonical_nodes =  T;

 }

blitz::Array<double,2> Grid::makeDomainNodes() {


  /*
    Affine transformation of a row vector

    from [-.5 x .5] x [-.5 x 0.5]

    to   [xa x xb]  x [ya x yb]

    p = [ -.5 .5 ] 

    M = [ xa xb
          ya yb ]

    p' = p * M' = [-.5 0.5] * [ xa ya; xb yb]

    p1 = -.5xa + 0.5xb
	  
    
   */
  auto domain = getDomain();
  auto nodes = getCanonicalNodes();
  double xa = domain[0].first;
  double xb = domain[0].second;
  double ya = domain[1].first;
  double yb = domain[1].second;
  
  blitz::Array<double,2> M(3,4);
  M = -.5 , .5 , .5 , -.5,
    -.5 , -.5, 0.5, 0.5,
    1.0 , 1.0 , 1.0, 1.0;

   blitz::Array<double,2> x(3,4);
   x = xa, xb, xb , xa,
       ya, ya, yb, yb,
       1.0 , 1.0 , 1.0, 1.0;

   // Tr = (inv(xi*xi')*xi*x')';
  blitz::Array<double,2> Mt = M.transpose(blitz::secondDim,
					    blitz::firstDim);


  blitz::Array<double,2> xt = x.transpose(blitz::secondDim,
					   blitz::firstDim);

  blitz::Array<double,2> xx(M.extent(blitz::firstDim),
			    Mt.extent(blitz::secondDim));

  blitz::Array<double,2> yy(M.extent(blitz::firstDim),
			    M.extent(blitz::secondDim));

  blitz::Array<double,2> zz(M.extent(blitz::firstDim),
			     M.extent(blitz::firstDim));


  
  
  interfaceCLAPACK::MatrixMult(M,Mt,xx);                // M * M'
  interfaceCLAPACK::MoorePenroseInverse(xx,xx);         // inv(M*M')
  interfaceCLAPACK::MatrixMult(xx,M,yy);                // inv(M*M') * M
  interfaceCLAPACK::MatrixMult(yy,xt,zz);               // (inv(M*M') * M) * x'

  blitz::Array<double,2> zzt = zz.transpose(blitz::secondDim,
					    blitz::firstDim);

   int n = nodes.size();

   blitz::Array<double,2> Tr(3,n);
   blitz::Array<double,2> TT(3,n);

   for (int i = 0 ; i < n ; i++){
     Tr(0,i) = nodes[i].first;
     Tr(1,i) = nodes[i].second;
     Tr(2,i) = 1.0;
   }

   interfaceCLAPACK::MatrixMult(zzt,Tr,TT);
   
   blitz::Array<double,2> ttt = TT.transpose(blitz::secondDim,blitz::firstDim);

   blitz::Array<double,2> domain_nodes(n,2);

   domain_nodes =  ttt(blitz::Range::all(),blitz::Range(0,1));

   // std::cout << domain_nodes << std::endl;
   
   return domain_nodes;
   
}

int Grid::getNumberOfNodes() const {
  return m_number_of_nodes;
}

