#include "matrixhelper.h"

namespace matrixhelper {

  blitz::Array<double,2> makeMatrixPosDef(const blitz::Array<double,2>& A){

    int n = A.rows();
    blitz::Array<double,2> B(n,n);
    B = A;

    blitz::Array<double,1> a(3);
    a=0.0;
    interfaceCLAPACK::calculateEigenvalues(A,a);

    bool all_eigs_pos = true;

    for (int i=0;i<n;i++) if(a(i)<0.0) all_eigs_pos = false;

    if (all_eigs_pos) return A;

    double nu = std::max(1.0,sqrt(n*n-1));

    std::vector<double> diag;
    std::vector<double> diag_off;

    for (int i=0;i<n;i++){
      diag.push_back(A(i,i));
	for (int j=0;j<n;j++){
	  if (i!=j) diag_off.push_back(A(i,j));
	}
    }
    double eps   =  10e-20;
    double gamma = *std::max_element(diag.begin(),diag.end());
    double xi    = *std::max_element(diag_off.begin(),diag_off.end());
    double beta_2= std::max(std::max(gamma,xi/nu),eps);

    blitz::Array<double,2> C = A;
    blitz::Array<double,2> L(n,n);
    blitz::Array<double,1> d(n);
    blitz::Array<double,1> E(n);
    d = 0.0;
    E = 0.0;

    for (int i=0;i<n;i++) L(i,i) = 1.0;

    for (int j=0;j<n;j++){
       // MC3
       std::vector<double> c_diag;

       for (int i=j;i<n;i++) c_diag.push_back(fabs(C(i,i)));

       std::vector<double>::iterator first = c_diag.begin();

       std::vector<double>::iterator it =
	 std::max_element(c_diag.begin(),c_diag.end());

       int q = std::distance(first,it);

       blitz::Array<double,2> P(n,n);
       for (int i=0; i<n; i++) P(i,i) = 1.0;
       if (q!=j){
	 //// Permutation Matrix
	 blitz::Array<double,1> q_row = P(q,blitz::Range::all());
	 P(q,blitz::Range::all()) = P(j,blitz::Range::all());
	 P(j,blitz::Range::all()) = q_row;
	 interfaceCLAPACK::MatrixMult(P,C,C);
       }

       // MC4:
       double theta_j = 0.0;
       double sum = 0.0;
       for (int s=0;s<j;s++) L(j,s) = C(j,s) / d(s);
       std::vector<double> theta_vec;
       for (int i=j+1 ; i<n;i++){
	 for (int s=0;s<j;s++) sum+=L(j,s)*C(i,s);
	 C(i,j) = C(i,j) - sum;
	 theta_vec.push_back(fabs(C(i,j)));
       }
       if (j!=n-1) {
	 theta_j =  *std::max_element(theta_vec.begin(),theta_vec.end());
       }

       // MC5
       d(j) = std::max(fabs(C(j,j)),(theta_j*theta_j)/beta_2);
       E(j) = d(j) - C(j,j);

       // MC6:
       for (int i=j+1;i<n;i++) C(i,i) = C(i,i) - (C(i,j) * C(i,j))/d(j);

    }

    for (int i=0;i<n;i++) B(i,i) += E(i);

    return B;

  } // end makeMatrixPosDef


  blitz::Array<double,2> makeMatrixPosDef(const blitz::Array<double,2>& A,
					  const blitz::Array<double,1>& a){

    int n = A.rows();
    blitz::Array<double,2> B(n,n);
    B = A;

    bool all_eigs_pos = true;

    for (int i=0;i<n;i++) if(a(i)<0.0) all_eigs_pos = false;

    if (all_eigs_pos) return A;

    double nu = std::max(1.0,sqrt(n*n-1));

    std::vector<double> diag;
    std::vector<double> diag_off;

    for (int i=0;i<n;i++){
      diag.push_back(A(i,i));
	for (int j=0;j<n;j++){
	  if (i!=j) diag_off.push_back(A(i,j));
	}
    }
    double eps   =  10e-20;
    double gamma = *std::max_element(diag.begin(),diag.end());
    double xi    = *std::max_element(diag_off.begin(),diag_off.end());
    double beta_2= std::max(std::max(gamma,xi/nu),eps);

    blitz::Array<double,2> C = A;
    blitz::Array<double,2> L(n,n);
    blitz::Array<double,1> d(n);
    blitz::Array<double,1> E(n);
    d = 0.0;
    E = 0.0;

    for (int i=0;i<n;i++) L(i,i) = 1.0;

    for (int j=0;j<n;j++){
       // MC3
       std::vector<double> c_diag;

       for (int i=j;i<n;i++) c_diag.push_back(fabs(C(i,i)));

       std::vector<double>::iterator first = c_diag.begin();

       std::vector<double>::iterator it =
	 std::max_element(c_diag.begin(),c_diag.end());

       int q = std::distance(first,it);

       blitz::Array<double,2> P(n,n);
       for (int i=0; i<n; i++) P(i,i) = 1.0;
       if (q!=j){
	 //// Permutation Matrix
	 blitz::Array<double,1> q_row = P(q,blitz::Range::all());
	 P(q,blitz::Range::all()) = P(j,blitz::Range::all());
	 P(j,blitz::Range::all()) = q_row;
	 interfaceCLAPACK::MatrixMult(P,C,C);
       }

       // MC4:
       double theta_j = 0.0;
       double sum = 0.0;
       for (int s=0;s<j;s++) L(j,s) = C(j,s) / d(s);
       std::vector<double> theta_vec;
       for (int i=j+1 ; i<n;i++){
	 for (int s=0;s<j;s++) sum+=L(j,s)*C(i,s);
	 C(i,j) = C(i,j) - sum;
	 theta_vec.push_back(fabs(C(i,j)));
       }
       if (j!=n-1) {
	 theta_j =  *std::max_element(theta_vec.begin(),theta_vec.end());
       }

       // MC5
       d(j) = std::max(fabs(C(j,j)),(theta_j*theta_j)/beta_2);
       E(j) = d(j) - C(j,j);

       // MC6:
       for (int i=j+1;i<n;i++) C(i,i) = C(i,i) - (C(i,j) * C(i,j))/d(j);

    }

    for (int i=0;i<n;i++) B(i,i) += E(i);

    return B;

  } // end makeMatrixPosDef


   /* Nullspace */

  blitz::Array<double,2> nullspace(const blitz::Array<double,2>& A){

    blitz::Array<double,2> U(A.rows(),A.rows());
    blitz::Array<double,1> S(std::min(A.rows(),A.columns()));
    blitz::Array<double,2> V(A.columns(),A.columns());

    int m = A.rows();
    int n = A.columns();

    U = 0.0;
    S = 0.0;
    V = 0.0;

    interfaceCLAPACK::SingularValueDecomposition(A,U,S,V);

    double tol = m > n ? m : n                           *
                 *std::max_element(S.begin(),S.end())    * 
                  2.220446049250313e-16 ;

    int r = 0;
    for (int i=0 ; i<S.extent(blitz::firstDim); i++) if (S(i) > tol) r+=1;
      
    int cols;
    if (r < n) {
      cols = n - r;
    } else {
      cols = 1;
    }
    
    blitz::Array<double,2> Z(n,cols);

    Z = V(blitz::Range::all(),blitz::Range(r,A.columns()-1));
    
    return Z;

  }


  /* Norm */
  double norm(const blitz::Array<double,2>& A){
    
    double norm = 0.0;

    for (int i=0; i<A.extent(blitz::firstDim) ; i++) norm+=A(i)*A(i);

    return sqrt(norm);

  }

  /* Norm */
  double norm(const blitz::Array<double,2>& A,
	      const blitz::Array<double,2>& B){

    double norm = 0.0;

    for (int i=0; i<A.extent(blitz::firstDim) ; i++) norm+=(A(i)-B(i))*(A(i)-B(i));

    return sqrt(norm);

  }

  bool isnan(double x) { return x != x; }

  double directionalDerivative(const blitz::Array<double,2>& d,
					       const blitz::Array<double,2>& p){

    double D = 0.0;

    if ( d.rows() != p.rows() ) return 0.0;

    for (int i = 0;i<d.rows() ; ++i) D += d(i) * p(i) ;

    return D;
    
  }
  
} // namespace matrixhelper
