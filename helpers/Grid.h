#ifndef GRID_H_
#define GRID_H_

#include <string>
#include <vector>

#include "InterfaceCLAPACK.h"

#include <blitz/array.h>

class Grid {
public:
Grid(const int& level,const std::vector<std::pair<double,double> > domain);
~Grid();

int getLevel() const;
double getDelta() const;
int getNumberOfNodes() const;
std::vector<double> getCanonicalVector() const;
std::vector<std::pair<double,double> > getCanonicalNodes() const;
blitz::Array<double,2> getDomainNodes() const;
std::vector<std::pair<int,int> > getIndexVector() const;
std::vector<std::pair<double,double> > getDomain() const;
void setCanonicalVector();
void setIndexVector();
blitz::Array<double,1> makeCanonicalArray();
void setCanonicalNodes();
blitz::Array<double,2> makeDomainNodes();


private:

int m_level;
double m_delta;
std::vector<double> m_canonical_vec;
std::vector<std::pair<int,int> > m_index_vec;
std::vector<std::pair<double,double> > m_domain;
std::vector<std::pair<double,double> > m_canonical_nodes;
int m_number_of_nodes;
 

};
#endif // GRID_H_
