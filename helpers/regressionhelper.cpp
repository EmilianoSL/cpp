#include "regressionhelper.h"

namespace regressionhelper {

blitz::Array<double,2>  polyfit(const blitz::Array<double,1>& x,
				const blitz::Array<double,1>& y,
				int degree){
  // (V.t * V) * b = V.t * y  
  int n = degree+1;
  blitz::Array<double,2> b(n,1);
  blitz::Array<double,2> o(y.extent(blitz::firstDim),1); 
  blitz::Array<double,2> V(x.extent(blitz::firstDim),n); 
  blitz::Array<double,2> X(n,n);
  blitz::Array<double,2> Y(n,1);
  b = 0.0;
  V = 0.0;
  X = 0.0;
  Y = 0.0;

  for (int i=0; i < x.extent(blitz::firstDim); i++){
    double v = 1.0;
    for(int j=0 ; j < n ; j++){
      V(i,j) = v; 
      v     *= x(i);
      o(i,0)   = y(i);
    }    
  }
  
  blitz::Array<double,2> Vt = V.transpose(blitz::secondDim,
					  blitz::firstDim);
  interfaceCLAPACK::MatrixMult(Vt,V,X);
  
  interfaceCLAPACK::MatrixMult(Vt,o,Y);

  interfaceCLAPACK::SolveLinear(X,b,Y);
  
  return b;

}

  blitz::Array<double,1> calcFittedValues(const blitz::Array<double,2>& betas,
					  const blitz::Array<double,1>& x){

    int n = x.extent(blitz::firstDim);
    int m = betas.extent(blitz::firstDim);
    blitz::Array<double,1> y_hat(n);
    y_hat=0.0;

    for (int i=0; i<n; i++) {
      double v = 1.0;
      for (int j=0; j<m; j++) { 
	y_hat(i) += betas(j,0) * v;
	v *= x(i);
      }
    }

    return y_hat;
  }

blitz::Array<double,1> calcError(const blitz::Array<double,1>& y,
				 const blitz::Array<double,1>& y_hat){

  blitz::Array<double,1> e(y.extent(blitz::firstDim));
  e = sqrt((y-y_hat)*(y-y_hat));
  return e;

}


  double std_dev(const blitz::Array<double,1>&x){
    int n = x.extent(blitz::firstDim);
    blitz::Array<double,1> m(1);
    m = mean(x);
    double std=0.0;
    for (int i=0; i < n ; i++) std += (x(i)-m(0))*(x(i)-m(0));
    std = sqrt(std/(n-1));
    return std;
  }

  double std_dev(const std::vector<double>& x){
    int n = x.size();
    double m = mean(x);
    double std = 0.0;
    for (int i=0; i < n ; i++) std += (x[i]-m)*(x[i]-m);
    std = sqrt(std/(n-1));
  return std;
}

  double mean(const std::vector<double>& x){

     int n = x.size();
     double sum = 0.0;
     for (int i=0; i<n; i++) sum+=x[i];
     return sum / n ;
  }

} // namespace regressionhelper
