#include "objfun.h"

double objfun(Portfolio& portfolio,
	      const blitz::Array<double,2>& C,
	      const blitz::Array<double,2>& W,
	      const blitz::Array<double,1>& T,
	      const blitz::Array<double,1>& P,
	      const TermStructure& term_structure) {

 blitz::Array<double,1> R = portfolio.makePricesDiff(term_structure,C,T);

 int n = W.rows();

 double f = 0.0;

 for (int i=0; i < n ; i++) f += 0.5 * R(i) * W(i,i) * R(i);

 return f;

}

double objfun(Portfolio& portfolio,
	      const blitz::Array<double,2>& x,
	      const blitz::Array<double,2>& C,
	      const blitz::Array<double,2>& W,
	      const blitz::Array<double,1>& T,
	      const blitz::Array<double,1>& P,
	      const boost::gregorian::date& start_date,
	      const boost::gregorian::date& end_date,
	      const CurveType& curve_type,
	      const ParametricForm& param_form,
	      const std::string& day_count
	      ){

  blitz::Array<double,2> params(6,1);

  params = x(0), x(1), x(2), x(3), x(4), x(5);

  TermStructureParametric term_structure(start_date,
					 end_date,
					 params,
					 param_form,
					 curve_type,
					 day_count);

  blitz::Array<double,1> R = portfolio.makePricesDiff(term_structure,C,T);

  int n = W.rows();

  double f = 0.0;

  for (int i=0; i < n ; i++) f += 0.5 * R(i) * W(i,i) * R(i);

  return f;
  
}

double objfun(
	      double x[6],
	      Portfolio& portfolio,
	      const blitz::Array<double,2>& C,
	      const blitz::Array<double,2>& W,
	      const blitz::Array<double,1>& T,
	      const blitz::Array<double,1>& P,
	      const boost::gregorian::date& start_date,
	      const boost::gregorian::date& end_date,
	      const CurveType& curve_type,
	      const ParametricForm& param_form,
	      const std::string& day_count
	      ) {
  
  blitz::Array<double,2> params(6,1);

  params = x[0], x[1], x[2], x[3], x[4], x[5];

  TermStructureParametric term_structure(start_date,
					 end_date,
					 params,
					 param_form,
					 curve_type,
					 day_count);

  blitz::Array<double,1> R = portfolio.makePricesDiff(term_structure,C,T);

  int n = W.rows();

  double f = 0.0;

  for (int i=0; i < n ; i++) f += 0.5 * R(i) * W(i,i) * R(i);

  return f;
  

}

double objfuncon(
	      double x[6],
	      Portfolio& portfolio,
	      const blitz::Array<double,2>& C,
	      const blitz::Array<double,2>& W,
	      const blitz::Array<double,1>& T,
	      const blitz::Array<double,1>& P,
	      const boost::gregorian::date& start_date,
	      const boost::gregorian::date& end_date,
	      const CurveType& curve_type,
	      const ParametricForm& param_form,
	      const std::string& day_count
	      ) {

  blitz::Array<double,2> params(6,1);
  // Introduce Penalty Functions

  std::vector<double> penalties;

  double mu = 100;
  double x0_min = -0.03;
  double x0x1 = -0.03;

  double x4_min = -0.5;
  double x4_max = 3.0;
  double x5_min = 0.1;
  double x5_max = 5.5;
  double x6_min = 5.5;
  double x6_max = 7.0;

  double cond1 = 0.0;
  double cond2 = 0.0;
  double cond3 = 0.0;
  double cond4 = 0.0;
  double cond5 = 0.0;
  double cond6 = 0.0;

  x[0] > x0_min ? cond1 = 0.0 : cond1 = ((x[0]-x0_min)*(x[0]-x0_min)) * mu;

  (x[0]+x[1]) > x0x1 ? cond2 = 0.0 : cond2 = ( ( x[0] + x[1] - x0x1) * ( x[0] + x[1] - x0x1 ) ) * mu;

  x[4] > x5_min ? cond3 = 0.0 : ( (x5_min - x[4]) * (x5_min - x[4]) ) * mu ;

  x[5] > x6_min ? cond4 = 0.0 : ( (x6_min - x[5]) * (x6_min - x[5]) ) * mu ;

  x5_max < x[4] ? cond5 = 0.0 : ( (x[4] - x5_max) * (x[4] - x5_max) ) * mu ;

  x6_max < x[5] ? cond6 = 0.0 : ( (x6_min - x[5]) * (x[5] - x6_max) ) * mu ;
  
  penalties.push_back(cond1);
  penalties.push_back(cond2);
  penalties.push_back(cond3);
  penalties.push_back(cond4);
  penalties.push_back(cond5);
  penalties.push_back(cond6);
  
  params = x[0], x[1], x[2], x[3], x[4], x[5];
 

  TermStructureParametric term_structure(start_date,
					 end_date,
					 params,
					 param_form,
					 curve_type,
					 day_count);

  blitz::Array<double,1> R = portfolio.makePricesDiff(term_structure,C,T);

  int n = W.rows();

  double f = 0.0;

  for (int i=0; i < n ; i++) f += 0.5 * R(i) * W(i,i) * R(i);

  for (int j=0; j<penalties.size() ; j++) f += penalties[j];

  return f;
  

}
  


double penalty(const blitz::Array<double,2>& x,double& x1_min){
  double out = 0.0;
  double mu  = 10.0;
  
  if ( x(4,0) > x(5,0)) out += ( ( x(4,0) - x(5,0) ) * ( x(4,0) - x(5,0) ) ) * mu;
  if ( x(1,0) + x(2,0) > x1_min ) out+= ( ( x(1,0) + x(2,0) - x1_min) * ( x(1,0) + x(2,0) - x1_min)  ) * mu; 

  return out;
}

