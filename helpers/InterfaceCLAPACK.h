
#ifndef INTERFACECLAPACK_H_
#define INTERFACECLAPACK_H_
#include <blitz/array.h>


namespace interfaceCLAPACK {
          

// Calculate Cholesky factorization of a real symmetric positive definite matrix.
void Cholesky(const blitz::Array<double,2>& A,
	      blitz::Array<double,2>& triangular,
	      char LorU);

void SingularValueDecomposition(const blitz::Array<double,2>& A,
				blitz::Array<double,2>& U,
				blitz::Array<double,1>& sigma,
				blitz::Array<double,2>& V);

// Solve symmetric eigenvalue problem.
void SymmetricEigenvalueProblem(const blitz::Array<double,2>& A,
				blitz::Array<double,1>& eigval,
				blitz::Array<double,2>& eigvec,
				double eps = 1e-12);

// Solve system of linear equations A X = B using CLAPACK routines.
void SolveLinear(const blitz::Array<double,2>& A,
		 blitz::Array<double,2>& X,
		 const blitz::Array<double,2>& B);

// Solve system of linear equations A X = B using CLAPACK routines, where A is a tridiagonal matrix.
void SolveTridiagonal(const blitz::Array<double,2>& A,
		      blitz::Array<double,2>& X,
		      const blitz::Array<double,2>& B);

void SolveTridiagonalSparse(const blitz::Array<double,2>& A,
			    blitz::Array<double,2>& X,
			    const blitz::Array<double,2>& B);

// Determinant of a real symmetric positive definite matrix.
double PositiveSymmetricMatrixDeterminant(const blitz::Array<double,2>& A);

// Inverse of a real symmetric positive definite matrix.
void PositiveSymmetricMatrixInverse(const blitz::Array<double,2>& A,
				    blitz::Array<double,2>& inverseA);

void MoorePenroseInverse(const blitz::Array<double,2>& A,
			 blitz::Array<double,2>& inverseA,
			 double eps = 1e-6);



// Matrix Multiplication A * B = C
void MatrixMult(const blitz::Array<double,2>& A,
		const blitz::Array<double,2>& B,
	        blitz::Array<double,2>& C);

void MatrixMult(const blitz::Array<double,2>& A,
		const blitz::Array<double,2>& B,
	        const blitz::Array<double,2>& C,
		blitz::Array<double,2>& D);

void MatrixMult(const blitz::Array<double,2>& A,
		const blitz::Array<double,1>& B,
	        blitz::Array<double,1>& C);


// Calculate Eigenvalues

 void calculateEigenvalues(const blitz::Array<double,2>& A,
			    blitz::Array<double,1>& eigen);

}
#endif // INTERFACE_CLAPACK_H_
