#ifndef MATRIXHELPER_H_
#define MATRIXHELPER_H_

#include <algorithm>
#include <math.h>
#include <vector>
#include <blitz/array.h>
#include "InterfaceCLAPACK.h"

namespace matrixhelper {

  blitz::Array<double,2> makeMatrixPosDef(const blitz::Array<double,2>& A);
  blitz::Array<double,2> makeMatrixPosDef(const blitz::Array<double,2>& A,
					  const blitz::Array<double,1>& e);
  blitz::Array<double,2> nullspace(const blitz::Array<double,2>& A);
  double norm(const blitz::Array<double,2>& A);
  double norm(const blitz::Array<double,2>& A,
	      const blitz::Array<double,2>& B);
  bool isnan(double x);

  double directionalDerivative(const blitz::Array<double,2>& d,
			       const blitz::Array<double,2>& p);

} //namespace matrixhelper
#endif //MATRIXHELPER_H_
