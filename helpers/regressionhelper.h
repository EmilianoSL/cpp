#ifndef REGRESSIONHELPER_H_
#define REGRESSIONHELPER_H_

#include <algorithm>
#include <math.h>
#include <vector>
#include <blitz/array.h>
#include <boost/function.hpp>
#include "InterfaceCLAPACK.h"

namespace regressionhelper {

  blitz::Array<double,2> polyfit(const blitz::Array<double,1>& x,
				 const blitz::Array<double,1>& y,
				 int degree = 1);

  blitz::Array<double,1> calcFittedValues(const blitz::Array<double,2>& betas,
					  const blitz::Array<double,1>& x);


  blitz::Array<double,1> calcError(const blitz::Array<double,1>& y,
				   const blitz::Array<double,1>& y_hat);

  double std_dev(const blitz::Array<double,1>&x);

  double std_dev(const std::vector<double>& x);
  double mean(const std::vector<double>& x);

} // namespace regressionhelper
#endif
