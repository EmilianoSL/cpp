#ifndef LEG_H_
#define LEG_H_

#include <string>
#include <vector>
#include <memory>

#include "../TermStructure/TermStructure.h"
#include "../helpers/timehelper.h"
#include "../helpers/conventionshelper.h"


class OneCashFlowFix;

class Leg {
 public:
  Leg(
      const std::vector<OneCashFlowFix> fixed_cashflows,
      const std::string day_count_convention_str);

 virtual~Leg();

 const std::vector<OneCashFlowFix>& getFixedCashFlows() const ;
 
 const std::string getDayCountConvention() const;
 
 double calculatePresentValue(const TermStructure& term_structure);

 double calculatePresentValueDerivative(const TermStructure& term_structure);

 virtual void calculatePresentValue(const TermStructure& term_structure,
				    double* result);

 std::string toString() const ;

 protected:
  std::vector<OneCashFlowFix> m_fixed_cashflows;
  std::string m_day_count_convention;

};
#endif // LEG_H_
  
