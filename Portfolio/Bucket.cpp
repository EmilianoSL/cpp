#include <sstream>
#include <ostream>
#include "Bucket.h"
#include <boost/lexical_cast.hpp>

Bucket::Bucket(
	       const boost::gregorian::date& date,
	       const double& value,
	       const double& time) :

    m_date(date),
    m_value(value),
    m_time(time)
{}

Bucket::~Bucket(){};

boost::gregorian::date Bucket::getDate() const {
  return m_date;
}

double Bucket::getValue() const {
  return m_value;
}

double Bucket::getTime() const{
  return m_time;
}

std::string Bucket::toString() const{
  std::ostringstream out;
 out << "Date: ";
 out << to_iso_extended_string(m_date) << "\r\n";
 out << "Value: ";
 out << boost::lexical_cast<std::string>(m_value) << "\r\n";
 out << "Time: ";
 out << boost::lexical_cast<std::string>(m_time) << "\r\n";
  return out.str();
}

 
