#ifndef ONECASHFLOWFIX_H_
#define ONECASHFLOWFIX_H_

#include <string>
#define BOOST_DATE_TIME_NO_LIB
#include <boost/date_time.hpp>


class OneCashFlowFix {
public:
OneCashFlowFix(
  const boost::gregorian::date& payment_date,
  const double amount,
  const std::string& currency);

virtual ~OneCashFlowFix();

boost::gregorian::date getPaymentDate() const;
double getAmount() const;
std::string getCurrency() const;

bool operator<(const OneCashFlowFix& right_hand_side) const {
 return m_payment_date < right_hand_side.m_payment_date;
}


std::string toString() const;

private:
boost::gregorian::date m_payment_date;
double m_amount;
std::string m_currency;

};
#endif // ONECASHFLOWFIX_H_
