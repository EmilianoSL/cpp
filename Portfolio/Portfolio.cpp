#include "Portfolio.h"


Portfolio::Portfolio(const boost::gregorian::date& reference_date)
  :
  m_reference_date(reference_date)
{};


Portfolio::Portfolio(const boost::gregorian::date& reference_date,
		     const std::string& country)
  :
  m_reference_date(reference_date),
  m_country(country)
{};

Portfolio::~Portfolio(){};

void Portfolio::setAll(){
  setPricesVector();
  setInstrumentCashflows();
  setNumberOfUniqueDates();
  setPricesArray();
  setYieldsArray();
}

void Portfolio::setOneBond(const Bond& bond) {
  m_bonds.push_back(boost::make_shared<Bond>(bond));
  m_portfolio.push_back(m_bonds.back());
}


void Portfolio::setCurrency(const std::string& currency){
  m_currency = currency;
}


void Portfolio::setCountry(const std::string& country){
  m_country = country;
}

std::string Portfolio::getCountry() const {
  return m_country;
}

std::string Portfolio::getCurrency() const {
  return m_currency;
}

void Portfolio::clearPortfolio(){
  m_portfolio.clear();
  m_bonds.clear();
}

boost::shared_ptr<Instrument> Portfolio::getOneInstrument(size_t i) {
  return m_portfolio[i];
}

int Portfolio::getPortfolioSize() const { 
  return m_portfolio.size();
}

void Portfolio::eraseOneInstrument(size_t i){
  m_portfolio.erase(m_portfolio.begin()+i);
}

void Portfolio::eraseInstrument(std::string& isin){
  for (int i = 0;i < m_portfolio.size();++i){
    std::string bond_code = getOneInstrument(i)->getBondCode();
    if (!bond_code.compare(isin)) eraseOneInstrument(i); 
  }
}

std::vector<boost::shared_ptr<Instrument> > Portfolio::getPortfolio() const {
  return m_portfolio;
}

boost::shared_ptr<Bond> Portfolio::getOneBond(size_t i) {
  return m_bonds[i];
}

blitz::Array<double,1> Portfolio::getTimeToMaturityArray() const {
  return  m_time_to_maturity_array;
}

boost::gregorian::date Portfolio::getReferenceDate() const {
  return m_reference_date;
}

std::vector<double>  Portfolio::getPricesVector() const {
  return m_prices_vector;
}

blitz::Array<double,1> Portfolio::getPricesArray() const {
  return m_prices_array;
}

blitz::Array<double,1> Portfolio::getYieldsArray() const {
  return m_yields_array;
}



std::vector<std::vector<OneCashFlowFix> > Portfolio::getInstrumentCashflows() const {
  return m_instrument_cashflows;
}

void Portfolio::setCashFlowMatrix() {
  m_cashflow_matrix = makeCashFlowMatrix();
}

blitz::Array<double,2> Portfolio::getCashFlowMatrix() const {
  return m_cashflow_matrix;
}

void Portfolio::setCashFlowDatesMatrix() {
  m_cashflow_matrix_dates = makeCashFlowDatesMatrix();
}

blitz::Array<boost::gregorian::date,2> Portfolio::getCashFlowDatesMatrix() const {
  return m_cashflow_matrix_dates;
}

double Portfolio::calculatePresentValue(const TermStructure& term_structure,
				        double* result) {

  double pv = 0.0;
  
  int n = getPortfolio().size();

  for (int i = 0 ; i < n ; i++){
     getOneInstrument(i)->calculatePresentValue(term_structure,result);
     pv += *result; 
  }
  return pv ;
}

blitz::Array<double,1> Portfolio::calculateYield(const blitz::Array<double,1>& prices){
  int n = getPortfolio().size();
  blitz::Array<double,1> yields(n);
  for (int i=0; i < n ;++i) {
    yields(i) = getOneInstrument(i)->calculateYield(prices(i));
  }
  return yields;
}


  double Portfolio::calculatePresentValue(const TermStructure& term_structure) {

    double pv = 0.0;
    
    return pv;
    
  }

void Portfolio::setPricesVector(){

  std::vector<double> prices;
  
  for (int i = 0 ; i < getPortfolio().size() ; i++){
    prices.push_back(getOneInstrument(i)->getPrice());
  }
  
  m_prices_vector = prices;
}

void Portfolio::setInstrumentCashflows() {

  int number_of_instruments = getPortfolio().size();

  std::vector<std::vector<OneCashFlowFix> > instrument_cashflows;

  for (int i = 0; i < number_of_instruments ; i++) {
     auto cashflows = getOneInstrument(i)->getLeg()->getFixedCashFlows();
     instrument_cashflows.push_back(cashflows);
  }
  m_instrument_cashflows = instrument_cashflows;
}

blitz::Array<double,1> Portfolio::makePricesArray() {

  const std::vector<double> prices = getPricesVector();
  blitz::Array<double,1> P(getPortfolio().size());
  for (int i = 0 ; i < getPortfolio().size() ; i++) {
    P(i) = prices[i];
  }
  return P;
}

void Portfolio::setPricesArray() {
  const std::vector<double> prices = getPricesVector();
  blitz::Array<double,1> P(getPortfolio().size());
  for (int i = 0 ; i < getPortfolio().size() ; i++) {
    P(i) = prices[i];
  }
  m_prices_array = P;

}

void Portfolio::setYieldsArray() {
  int n = getPortfolio().size();
  blitz::Array<double,1> Y(n);
  for (int i=0;i<n;++i) Y(i) = getOneInstrument(i)->getYield();
  m_yields_array = Y;
}


std::pair<blitz::Array<double,2>,blitz::Array<double,2>>
  Portfolio::makeMatrixPair(){

  std::pair<blitz::Array<double,2>,blitz::Array<double,2>> matrixPair;
  
  const std::vector<std::vector<OneCashFlowFix> > instrument_cashflows = getInstrumentCashflows();
  std::vector<boost::gregorian::date> dates_vector;
  int number_of_instruments = getPortfolioSize();
  for (int i = 0 ; i < number_of_instruments ; i++) {
    for (int j = 0 ; j < instrument_cashflows[i].size() ; j++){
      dates_vector.push_back(instrument_cashflows[i][j].getPaymentDate());
    }
  }

  std::sort(dates_vector.begin(),dates_vector.end());
  dates_vector.erase(std::unique(dates_vector.begin(),dates_vector.end()),dates_vector.end());

  std::set<boost::gregorian::date> dates_set(dates_vector.begin(),dates_vector.end());
  
  blitz::Array<double,2> cashflow_matrix(getPortfolioSize(),getNumberOfUniqueDates());
  blitz::Array<double,2> time_to_maturity_matrix(getPortfolioSize(),getNumberOfUniqueDates());
  
  cashflow_matrix = 0.0;
  
  for (int i = 0 ; i < number_of_instruments ; i++){
    for (int j = 0 ; j < instrument_cashflows[i].size(); j++){
      auto date = instrument_cashflows[i][j].getPaymentDate();
      std::set<boost::gregorian::date>::iterator first = dates_set.begin();
      std::set<boost::gregorian::date>::iterator it = dates_set.find(date);
      int index = std::distance(first,it);
      cashflow_matrix(i,index) = instrument_cashflows[i][j].getAmount();
      time_to_maturity_matrix(i,index) = 0.0;
    }
  }

  matrixPair = std::make_pair(cashflow_matrix,time_to_maturity_matrix);
  
  return matrixPair;
}


void Portfolio::setNumberOfUniqueDates() {
  
  auto dates_vector = makeUniqueDatesDayCountConventions();

  m_number_of_unique_dates = dates_vector.size();

}


int Portfolio::getNumberOfUniqueDates() const {
  return m_number_of_unique_dates;
}


std::vector<boost::gregorian::date> Portfolio::makeDatesVector(){

const std::vector<std::vector<OneCashFlowFix> > 
  instrument_cashflows = getInstrumentCashflows();

 std::vector<boost::gregorian::date> dates_vector;
 int n = getPortfolioSize();

 for (int i = 0 ; i < n ; i++) {
    for (int j = 0 ; j < instrument_cashflows[i].size() ; j++){
      dates_vector.push_back(instrument_cashflows[i][j].getPaymentDate());
    }
  }

std::sort(dates_vector.begin(),dates_vector.end());
  dates_vector.erase(std::unique(dates_vector.begin(),dates_vector.end()),dates_vector.end());

  return dates_vector;
}


blitz::Array<double,2>
Portfolio::makeCashFlowMatrix(const std::vector<std::pair<boost::gregorian::date,std::string> >& dates_vector) {

   const std::vector<std::vector<OneCashFlowFix> > 
     instrument_cashflows = getInstrumentCashflows();

   std::set<std::pair<boost::gregorian::date,std::string>> 
    dates_set(dates_vector.begin(),dates_vector.end());
  
  blitz::Array<double,2> cashflow_matrix(getPortfolioSize(),getNumberOfUniqueDates());

  cashflow_matrix = 0.0;
  
   int number_of_instruments = getPortfolioSize();
  for (int i = 0 ; i < number_of_instruments ; i++){
    std::string day_count = getOneInstrument(i)->getDayCountConvention();
    for (int j = 0 ; j < instrument_cashflows[i].size(); j++){
      auto date = instrument_cashflows[i][j].getPaymentDate();
      auto pair = std::make_pair(date,day_count);
      std::set<std::pair<boost::gregorian::date,std::string> >::iterator 
	first = dates_set.begin();
      std::set<std::pair<boost::gregorian::date,std::string> >::iterator 
	it = dates_set.find(pair);
      int index = std::distance(first,it);
      cashflow_matrix(i,index) = instrument_cashflows[i][j].getAmount();
    }
  }

  return cashflow_matrix;
}


blitz::Array<double,2> Portfolio::makeCashFlowMatrix() {

   const std::vector<std::vector<OneCashFlowFix> > 
     instrument_cashflows = getInstrumentCashflows();
  
   auto dates_vector = makeUniqueDatesDayCountConventions();

   std::set<std::pair<boost::gregorian::date,std::string>> 
    dates_set(dates_vector.begin(),dates_vector.end());
  
  blitz::Array<double,2> cashflow_matrix(getPortfolioSize(),getNumberOfUniqueDates());

  cashflow_matrix = 0.0;
  
   int number_of_instruments = getPortfolioSize();
  for (int i = 0 ; i < number_of_instruments ; i++){
    std::string day_count = getOneInstrument(i)->getDayCountConvention();
    for (int j = 0 ; j < instrument_cashflows[i].size(); j++){
      auto date = instrument_cashflows[i][j].getPaymentDate();
      auto pair = std::make_pair(date,day_count);
      std::set<std::pair<boost::gregorian::date,std::string> >::iterator 
	first = dates_set.begin();
      std::set<std::pair<boost::gregorian::date,std::string> >::iterator 
	it = dates_set.find(pair);
      int index = std::distance(first,it);
      cashflow_matrix(i,index) = instrument_cashflows[i][j].getAmount();
    }
  }

  return cashflow_matrix;
}


blitz::Array<boost::gregorian::date,2> Portfolio::makeCashFlowDatesMatrix(const std::vector<std::pair<boost::gregorian::date,std::string> >& dates_vector){

  
  const std::vector<std::vector<OneCashFlowFix> > 
     instrument_cashflows = getInstrumentCashflows();

   std::set<std::pair<boost::gregorian::date,std::string>> 
    dates_set(dates_vector.begin(),dates_vector.end());
 
  blitz::Array<boost::gregorian::date,2> 
    dates_matrix(getPortfolioSize(),getNumberOfUniqueDates());

   int number_of_instruments = getPortfolioSize();
  for (int i = 0 ; i < number_of_instruments ; i++){
    std::string day_count = getOneInstrument(i)->getDayCountConvention();
    for (int j = 0 ; j < instrument_cashflows[i].size(); j++){
      auto date = instrument_cashflows[i][j].getPaymentDate();
      auto pair = std::make_pair(date,day_count);
      std::set<std::pair<boost::gregorian::date,std::string>>::iterator 
	first = dates_set.begin();
      std::set<std::pair<boost::gregorian::date,std::string>>::iterator 
	it = dates_set.find(pair);
      int index = std::distance(first,it);
      dates_matrix(i,index) = date;
    }
  }
  
  return dates_matrix;
}


blitz::Array<boost::gregorian::date,2> Portfolio::makeCashFlowDatesMatrix(){

  
  const std::vector<std::vector<OneCashFlowFix> > 
     instrument_cashflows = getInstrumentCashflows();
  
   auto dates_vector = makeUniqueDatesDayCountConventions();

   std::set<std::pair<boost::gregorian::date,std::string>> 
    dates_set(dates_vector.begin(),dates_vector.end());
 
  blitz::Array<boost::gregorian::date,2> 
    dates_matrix(getPortfolioSize(),getNumberOfUniqueDates());

   int number_of_instruments = getPortfolioSize();
  for (int i = 0 ; i < number_of_instruments ; i++){
    std::string day_count = getOneInstrument(i)->getDayCountConvention();
    for (int j = 0 ; j < instrument_cashflows[i].size(); j++){
      auto date = instrument_cashflows[i][j].getPaymentDate();
      auto pair = std::make_pair(date,day_count);
      std::set<std::pair<boost::gregorian::date,std::string>>::iterator 
	first = dates_set.begin();
      std::set<std::pair<boost::gregorian::date,std::string>>::iterator 
	it = dates_set.find(pair);
      int index = std::distance(first,it);
      dates_matrix(i,index) = date;
    }
  }
  
  return dates_matrix;
}





blitz::Array<double,2> Portfolio::makeTimeToMaturityMatrix() {

     
  const std::vector<std::vector<OneCashFlowFix> > 
     instrument_cashflows = getInstrumentCashflows();
  
   auto dates_vector = makeUniqueDatesDayCountConventions();

   std::set<std::pair<boost::gregorian::date,std::string>> 
    dates_set(dates_vector.begin(),dates_vector.end());
 

   blitz::Array<double,2> time_to_maturity_matrix(getPortfolioSize(),
						   getNumberOfUniqueDates());
  
   time_to_maturity_matrix = 0.0;

   int number_of_instruments = getPortfolioSize();
 
   for (int i = 0 ; i < number_of_instruments ; i++){
  
     std::string day_count = getOneInstrument(i)->getDayCountConvention();
     
     const conventionshelper::DayCountConvention day_count_convention =
      conventionshelper::getStrDayCountConventionAsEnum(day_count);
    
     for (int j = 0 ; j < instrument_cashflows[i].size(); j++){
       
       auto date = instrument_cashflows[i][j].getPaymentDate();
       auto pair = std::make_pair(date,day_count);
      
       std::set<std::pair<boost::gregorian::date,std::string>>::iterator 
	first = dates_set.begin();
       
       std::set<std::pair<boost::gregorian::date,std::string>>::iterator 
	it = dates_set.find(pair);
      
       int index = std::distance(first,it);
   
       time_to_maturity_matrix(i,index) =
	 conventionshelper::calculateYearFraction(getReferenceDate(),
						  date,
						  day_count_convention);	 

    }
  }

 
  return time_to_maturity_matrix;

}

blitz::Array<double,1> Portfolio::matrixPresentValue(const TermStructure& term_structure,
						     const blitz::Array<double,2>& cashflows,
						     const blitz::Array<boost::gregorian::date,2>& dates) {

  int n = getPortfolioSize();
  int m = getNumberOfUniqueDates();
  
  blitz::Array<double,2> C(n,m);
  blitz::Array<double,2> D(n,m);
  blitz::Array<boost::gregorian::date,2> T(n,m);

  C = cashflows;
  T = dates;
  
   for (int i = 0 ; i < n ; i++) {
     std::string day_count = getOneInstrument(i)->getDayCountConvention();
     for (int j = 0 ; j < m ; j++) {
       D(i,j) = term_structure.getDiscountFactor(T(i,j),day_count);
     }
   }

   blitz::Array<double,2> P(C*=D);
   blitz::secondIndex j;
   blitz::Array<double,1> S(P.extent(blitz::firstDim));
   S = sum(P,j);

   return S;
}


blitz::Array<double,1> Portfolio::matrixPresentValue(const TermStructure& term_structure,
						     const blitz::Array<double,2>& cashflows,
						     const blitz::Array<double,1>& year_fractions) {

  int n = cashflows.rows();
  int m = cashflows.columns();
  
  blitz::Array<double,2> C(n,m);
  blitz::Array<double,2> D(n,m);
  
  C = cashflows;

  blitz::Array<double,1> S(n); S = 0.0;

  for (int i = 0 ; i < n ; i++) {
    double s = 0.0;
    for (int j = 0 ; j < m ; j++) {
      s+= term_structure.getDiscountFactor(year_fractions(j)) * C(i,j);
    }
    S(i) = s;
  }

   return S;
}


blitz::Array<double,1> Portfolio::matrixPresentValue(const TermStructure& term_structure) {

  int n = getPortfolioSize();
  int m = getNumberOfUniqueDates();
  
  blitz::Array<double,2> C(n,m);
  blitz::Array<double,2> D(n,m);
  blitz::Array<boost::gregorian::date,2> T(n,m);

  C = makeCashFlowMatrix();
  T = makeCashFlowDatesMatrix();
  
   for (int i = 0 ; i < n ; i++) {
     std::string day_count = getOneInstrument(i)->getDayCountConvention();
     for (int j = 0 ; j < m ; j++) {
       D(i,j) = term_structure.getDiscountFactor(T(i,j),day_count);
     }
   }

   blitz::Array<double,2> P(C*=D);
   blitz::secondIndex j;
   blitz::Array<double,1> S(P.extent(blitz::firstDim));
   S = sum(P,j);

   return S;
}

double Portfolio::calcCostFunctionValue(const blitz::Array<double,2>& weight_matrix,
			       const blitz::Array<double,1>& prices_diff){

  double sum = 0.0;
  for (int i=0; i < getPortfolioSize(); i++){
    sum += 0.5 * prices_diff(i) * weight_matrix(i,i) * prices_diff(i);
  }
  return sqrt(sum);
}

double Portfolio::calcCostFunctionValueYields(const blitz::Array<double,2>& weight_matrix,
					      const blitz::Array<double,1>& yields_diff){

  double sum = 0.0;
  for (int i=0; i < getPortfolioSize(); i++){
    sum += 0.5 * yields_diff(i) * weight_matrix(i,i) * yields_diff(i);
  }
  return sqrt(sum);

}


// std::string Portfolio::toString() const {

//   std::ostringstream out;

//    int n = getPortfolio().size();

//    // for (int i = 0 ; i < n ; i++){out << getOneBond(i)->toString();}

//   return out.str();
// }



blitz::Array<double,1> Portfolio::makeYieldsDiff(const TermStructure& term_structure) {

  int n = getPortfolioSize();
  blitz::Array<double,1> yields_obs = getYieldsArray();
  blitz::Array<double,1> yields_calc(n);
  blitz::Array<double,1> present_values(n); 
  present_values = matrixPresentValue(term_structure);
  for (int i=0;i<n;++i) yields_calc(i) = getOneInstrument(i)->calculateYield(present_values(i));
  blitz::Array<double,1> diff_array(n);
  diff_array = 0.0;
  diff_array = yields_obs - yields_calc;
  return diff_array;
}


blitz::Array<double,1> Portfolio::makePricesDiff(const TermStructure& term_structure) {

  int n = getPortfolioSize();

  blitz::Array<double,1> present_values(n);
  present_values = matrixPresentValue(term_structure);

  blitz::Array<double,1> bond_prices(n);
  bond_prices = makePricesArray();

  blitz::Array<double,1> diff_array(n);
  diff_array = 0.0;
  diff_array = present_values - bond_prices;

  return diff_array;

}

blitz::Array<double,1> Portfolio::makePricesDiff(const TermStructure& term_structure,
					const blitz::Array<double,2>& cashflows,
					const blitz::Array<double,1>& year_fractions){

  int n = getPortfolioSize();

  blitz::Array<double,1> present_values(n);
  present_values = matrixPresentValue(term_structure, cashflows, year_fractions);

  blitz::Array<double,1> bond_prices(n);
  bond_prices = makePricesArray();

  blitz::Array<double,1> diff_array(n);
  diff_array = 0.0;
  diff_array = present_values - bond_prices;

  return diff_array;

}


blitz::Array<double,2> Portfolio::makeWeightMatrix(){
  
  int n = getPortfolioSize();
  blitz::Array<double,2> W(n,n);
  W = 0.0;
  double total = 0.0;
  for (int i=0; i<n ; i++) total += 1/getOneInstrument(i)->calculateTimeToMaturity();
  for (int i=0; i<n ; i++){
    W(i,i) = ( 1/ getOneInstrument(i)->calculateTimeToMaturity() ) / total;
  }

  return W;
}

blitz::Array<double,2> Portfolio::makeWeightMatrixD(){
  
  int n = getPortfolioSize();
  blitz::Array<double,2> W(n,n);
  W = 0.0;
  double total = 0.0;
  for (int i=0; i<n ; i++) total += 1/getOneInstrument(i)->getDuration();
  for (int i=0; i<n ; i++) W(i,i) = ( 1/ getOneInstrument(i)->getDuration()) / total;
  // check weights
  double proportional = 1.0 / getPortfolioSize() ;
  bool outweight = false;
  for (int i=0; i<n ; i++) if (W(i,i) > proportional*4) outweight = true;
  if (outweight) for (int i=0; i<n ; i++) W(i,i) = proportional;
  return W;
}

void Portfolio::setUniqueDatesDaycounts() {

  std::vector<std::vector<OneCashFlowFix> > 
     instrument_cashflows = getInstrumentCashflows();

    int number_of_instruments = getPortfolioSize();

   std::vector<std::pair<boost::gregorian::date,std::string> > pairs_vector;
 
  for (int i = 0 ; i < number_of_instruments ; i++) {
    std::string day_count = getOneInstrument(i)->getDayCountConvention();
    for (int j = 0 ; j < instrument_cashflows[i].size() ; j++){
      boost::gregorian::date date = instrument_cashflows[i][j].getPaymentDate();
      auto pair = std::make_pair(date,day_count);
      pairs_vector.push_back(pair);
    }
  }

  std::sort(pairs_vector.begin(),pairs_vector.end());
  pairs_vector.erase(std::unique(pairs_vector.begin(),
				 pairs_vector.end()),
		     pairs_vector.end());


  m_unique_dates_daycounts = pairs_vector;
}

std::vector<std::pair<boost::gregorian::date,std::string> > 
Portfolio::makeUniqueDatesDayCountConventions(){

   std::vector<std::vector<OneCashFlowFix> > 
     instrument_cashflows = getInstrumentCashflows();

    int number_of_instruments = getPortfolioSize();

   std::vector<std::pair<boost::gregorian::date,std::string> > pairs_vector;
 
  for (int i = 0 ; i < number_of_instruments ; i++) {
    std::string day_count = getOneInstrument(i)->getDayCountConvention();
    for (int j = 0 ; j < instrument_cashflows[i].size() ; j++){
      boost::gregorian::date date = instrument_cashflows[i][j].getPaymentDate();
      auto pair = std::make_pair(date,day_count);
      pairs_vector.push_back(pair);
    }
  }

  std::sort(pairs_vector.begin(),pairs_vector.end());
  pairs_vector.erase(std::unique(pairs_vector.begin(),
				 pairs_vector.end()),
		     pairs_vector.end());

  return pairs_vector;

}

void Portfolio::setTimeToMaturityArray() {
  auto pairs_vector = makeUniqueDatesDayCountConventions();

  int n = pairs_vector.size();

  blitz::Array<double,1> t(n);

  auto ref_date = getReferenceDate();

  for (int i=0; i<n ; i++){

    std::string day_count = pairs_vector[i].second;
    const conventionshelper::DayCountConvention day_count_convention =
      conventionshelper::getStrDayCountConventionAsEnum(day_count);
    auto date = pairs_vector[i].first;
    
    double ttm = conventionshelper::calculateYearFraction(ref_date,
						  date,
						  day_count_convention);

    if ( ttm > 0 ) { 
      t(i) = ttm;
    } else {
      t(i) = 1.0 / 365.0;
    }

  }
  m_time_to_maturity_array = t;
}
  

blitz::Array<double,1> Portfolio::makeTimeToMaturityArray() {

  auto pairs_vector = makeUniqueDatesDayCountConventions();

  int n = pairs_vector.size();

  blitz::Array<double,1> t(n);

  auto ref_date = getReferenceDate();

  for (int i=0; i<n ; i++){

    std::string day_count = pairs_vector[i].second;
    const conventionshelper::DayCountConvention day_count_convention =
      conventionshelper::getStrDayCountConventionAsEnum(day_count);
    auto date = pairs_vector[i].first;
    
    double ttm = conventionshelper::calculateYearFraction(ref_date,
						  date,
						  day_count_convention);

    if ( ttm > 0 ) { 
      t(i) = ttm;
    } else {
      t(i) = 1.0 / 365.0;
    }

  }
  return t;
}

blitz::Array<double,1> Portfolio::makeTimeToMaturities(){
 
  int n = getPortfolioSize();
  blitz::Array<double,1> t(n);

  for (int i=0;i<n;i++) 
    t(i) = getOneInstrument(i)->calculateTimeToMaturity();
    
  return t;
}

blitz::Array<std::string,1> Portfolio::makeMaturityBrackets(const std::vector<std::pair<double,double>>& brackets){

  int n = getPortfolioSize();
  blitz::Array<std::string,1> b(n);

  for (int i=0;i<n;i++) 
    b(i) = getOneInstrument(i)->assignMaturityBracket(brackets);
    
  return b;

}

blitz::Array<double,1> Portfolio::calcAverageYieldByBracket(const std::vector<std::pair<double,double>>& brackets){
  int n = getPortfolioSize();
  int m = brackets.size();
  blitz::Array<double,1> avg_yield_by_bracket(m);

  for (int k=0; k<m; k++){
    double sum = 0.0;
    int count  = 0.0;
    for (int i=0;i<n;i++){   
      std::string bracket = "(" + std::to_string(brackets[k].first)
	+ "," + std::to_string(brackets[k].second) + "]";
      std::string bracket_instrument = getOneInstrument(i)->assignMaturityBracket(brackets);
      if (!bracket.compare(bracket_instrument) ) {
	sum += getOneInstrument(i)->getYield();
	count++;}
    }
    avg_yield_by_bracket(k) = (sum / count);
  }
    
  return avg_yield_by_bracket;
}


 blitz::Array<double,1> Portfolio::calcStdDevYieldByBracket(const std::vector<std::pair<double,double>>& brackets){

  int n = getPortfolioSize();
  int m = brackets.size();
  blitz::Array<double,1> avg_yield_by_bracket(m);
  blitz::Array<double,1> std_yield_by_bracket(m);

  for (int k=0; k<m; k++){
    double sum = 0.0;
    int count  = 0.0;
    for (int i=0;i<n;i++){   
      std::string bracket = "(" + std::to_string(brackets[k].first)
	+ "," + std::to_string(brackets[k].second) + "]";
      std::string bracket_instrument = getOneInstrument(i)->assignMaturityBracket(brackets);
      if (!bracket.compare(bracket_instrument) ) {
	sum += getOneInstrument(i)->getYield();
	count++;}
    }
    avg_yield_by_bracket(k) = (sum / count);
  }

  for (int k=0; k<m; k++){
    double sum = 0.0;
    int count  = 0.0;
    for (int i=0;i<n;i++){   
      std::string bracket = "(" + std::to_string(brackets[k].first)
	+ "," + std::to_string(brackets[k].second) + "]";
      std::string bracket_instrument = getOneInstrument(i)->assignMaturityBracket(brackets);
      if (!bracket.compare(bracket_instrument) ) {
	double y = getOneInstrument(i)->getYield();
	double mean = avg_yield_by_bracket(k);
	sum += (y-mean)*(y-mean);
	count++;}
    }
    std_yield_by_bracket(k) =  sqrt(sum)/sqrt(count-1);
  } 
    
  return std_yield_by_bracket;
}

std::map<std::string,std::pair<double,double>> Portfolio::makeMapYieldByBracket(const std::vector<std::pair<double,double>>& brackets){

  blitz::Array<double,1> avg_yield_by_bracket = calcAverageYieldByBracket(brackets);
  blitz::Array<double,1> std_yield_by_bracket = calcStdDevYieldByBracket(brackets);

  int m = brackets.size();
  std::map<std::string,std::pair<double,double>> bracket_yield_map;
  
  for (int k=0; k<m; k++){
    std::string bracket = "(" + std::to_string(brackets[k].first) + "," + std::to_string(brackets[k].second) + "]";
    bracket_yield_map.insert(
			     std::pair<std::string,std::pair<double,double>>(
									     bracket,std::make_pair(avg_yield_by_bracket(k),std_yield_by_bracket(k))));
  }
  
      return bracket_yield_map;
}

blitz::Array<double,2>
Portfolio::makeJacobianCostFunction(const blitz::Array<double,2>& C,
				    const blitz::Array<double,2>& A,
				    const blitz::Array<double,1>& T,
				    const blitz::Array<double,2>& x){

  int num_of_deals = C.extent(blitz::firstDim);
  int num_of_dates = T.extent(blitz::firstDim);

  blitz::Array<double,2> J(num_of_deals,4);

  blitz::Array<double,2> B(A.extent(blitz::firstDim),
			   A.extent(blitz::secondDim));

  blitz::Array<double,1> aux(A.extent(blitz::firstDim));

  J = 0.0;

  aux = 0.0;

  B = A * -1.0;

  for (int i=0; i<B.extent(blitz::firstDim); i++){
    for(int j=0; j<B.extent(blitz::secondDim); j++){
      aux(i) += B(i,j) * x(j,0);
    }
  }

  blitz::firstIndex i;

  aux = exp(aux(i) * T(i)) * T(i);

  for (int k=0 ; k < 4 ; k++){

    blitz::Array<double,1> J_col(num_of_deals);

    blitz::Array<double,1> A_col(T.extent(blitz::firstDim));

    blitz::Array<double,1> AA(T.extent(blitz::firstDim));

    J_col = 0.0;

    A_col = A(blitz::Range::all(),k);

    AA = aux(i) * A_col(i);

    for (int row = 0 ; row < num_of_deals ; row++){
      for (int col = 0 ; col < num_of_dates; col++){
	J_col(row) += C(row,col) * AA(col) ;
      }
    }

    J(blitz::Range::all(),k) = J_col;

  }
  return J;
}


blitz::Array<double,2> 
Portfolio::makeGradientCostFunction(const blitz::Array<double,2>& J,
				    const blitz::Array<double,2>& W,
				    const blitz::Array<double,1>& R){

  int m = getPortfolioSize();
  blitz::Array<double,2> G(4,1);
  blitz::Array<double,2> J_t = J.transpose(blitz::secondDim,
					    blitz::firstDim);

  blitz::Array<double,2> aux1(4,m);
  aux1 = 0.0;
  interfaceCLAPACK::MatrixMult(J_t,W,aux1);
   G = 0.0;

   for (int i=0 ; i<4 ; i++){
     for (int j=0 ;j<m; j++){
       G(i) += aux1(i,j) * R(j);
     }
   }

  return G;
}

blitz::Array<double,2> 
Portfolio::makeHessianCostFunction(const blitz::Array<double,2>& C,
				   const blitz::Array<double,2>& A,
				   const blitz::Array<double,2>& J,
				   const blitz::Array<double,2>& W,
				   const blitz::Array<double,1>& T,
				   const blitz::Array<double,1>& R,
				   const blitz::Array<double,2>& x){
  int m = C.rows();
  int n = C.columns();
  blitz::Array<double,1> E(n);
  blitz::Array<double,1> D(n);
  blitz::Array<double,2> H(4,4); H = 0.0;
  blitz::Array<double,1> x_row(x.extent(blitz::firstDim));
  x_row = x(blitz::Range::all(), 0);
  for (int i=0; i < n ; i++){
    blitz::Array<double,1> C_col(m);
    blitz::Array<double,1> A_row(A.extent(blitz::secondDim));
    C_col = C(blitz::Range::all(), i);
    A_row = A(i,blitz::Range::all());
    E(i) = sum( R * C_col) * -1.0;
    D(i) = exp( sum(A_row*x_row) * -1.0 * T(i) ) * T(i) * T(i);
  } 

  for (int i=0 ; i<4 ; i++){
    for (int k=0 ; k<4;k++){
      for (int j=0 ; j < T.extent(blitz::firstDim); j++){
	H(i,k) += E(j) * D(j) * A(j,i) * A(j,k);
      }
    }
  }

   blitz::Array<double,2> J_t = J.transpose(blitz::secondDim,blitz::firstDim);

   blitz::Array<double,2> aux(4,m);
   aux=0.0;

   interfaceCLAPACK::MatrixMult(J_t,W,aux);

   blitz::Array<double,2> aux2(4,4);
   aux2=0.0;

   interfaceCLAPACK::MatrixMult(aux,J,aux2);
   
   blitz::Array<double,2> ret(4,4);

   ret = H + aux2;

  return ret;

}


 std::tuple<blitz::Array<double,1>,
	    blitz::Array<double,1>,
	    blitz::Array<double,2>,
	    blitz::Array<double,2>,
	    blitz::Array<boost::gregorian::date,2> > Portfolio::makeAllMatrices(){

   int num_bonds = getPortfolioSize();
   int num_dates = getNumberOfUniqueDates();
   blitz::Array<double,1> P(num_bonds);
   blitz::Array<double,1> T(num_dates);
   blitz::Array<double,2> C(num_bonds,num_dates);   
   blitz::Array<double,2> W(num_bonds,num_bonds);
   blitz::Array<boost::gregorian::date,2> D(num_bonds,num_dates);
   P = makePricesArray();
   T = makeTimeToMaturityArray();
   C = makeCashFlowMatrix();
   W = makeWeightMatrix();
   D = makeCashFlowDatesMatrix();

   return std::make_tuple(P,T,C,W,D);
 }

blitz::Array<double,1> Portfolio::fitQuadraticToYields(){

   int n = m_portfolio.size();
   int m = 3;

  blitz::Array<double,1> t = makeTimeToMaturities();
  blitz::Array<double,2> X(n,m);

  for (int i=0;i<n;i++){
    X(i,0) = 1.0;
    X(i,1) = t(i);
    X(i,2) = t(i) * t(i);
  }

  blitz::Array<double,2> Xt = X.transpose(blitz::secondDim,
  					  blitz::firstDim);

  blitz::Array<double,2> XX(m,m);
  XX = 0.0;
  interfaceCLAPACK::MatrixMult(Xt,X,XX);
  
  blitz::Array<double,2> XXinv(m,m);
  XXinv = 0.0;

  interfaceCLAPACK::MoorePenroseInverse(XX,XXinv);

  blitz::Array<double,2> Hat(m,n);
  Hat = 0.0;
  
  interfaceCLAPACK::MatrixMult(XXinv,Xt,Hat);

  blitz::Array<double,1> beta(m);
  
  blitz::Array<double,1> yields(n);

  for (int i=0; i<n ; i++) yields(i) =  getOneInstrument(i)->getYield();

  for (int i=0; i<m; i++){
    for (int j=0; j<n; j++){
      beta(i) += Hat(i,j) * yields(j);
    }
  }

  return beta;

}

blitz::Array<double,1> Portfolio::getPortfolioYields(){
  int n = m_portfolio.size();
  blitz::Array<double,1> yields(n);
  for (int i=0; i<n ; i++) yields(i) =  getOneInstrument(i)->getYield();
  return yields;

}

double Portfolio::calcAverageYield(){
  
  double sum = 0.0;
  int n = getPortfolioSize();
  for (int i=0; i<n; i++) sum += getOneInstrument(i)->getYield();
  
  return sum / n;

}

double Portfolio::calcStdDevYields(){
  
  double m = calcAverageYield();
  int n = getPortfolioSize();
  double sum = 0.0;
  for (int i=0; i<n; i++) {
    double y = getOneInstrument(i)->getYield();
    sum += (y-m) * (y-m);
  }
  return sqrt(sum/(n-1));
}

void Portfolio::removeOutliers(const std::vector<std::pair<double,double>>& brackets, std::ofstream& outfile){
  std::vector<std::string> to_erase;
  std::map<std::string,std::pair<double,double>> map = makeMapYieldByBracket(brackets);
  for (int j=0; j < getPortfolioSize() ; ++j){
    auto it = map.find(getOneInstrument(j)->assignMaturityBracket(brackets));
    double t = getOneInstrument(j)->calculateTimeToMaturity();
    double y = getOneInstrument(j)->getYield();
    double lower = it->second.first + (it->second.second) * -2.0;
    double upper = it->second.first + (it->second.second) * 2.0;
    if (y> upper || y < lower){
      std::string bond_code =  getOneInstrument(j)->getBondCode();
      outfile << getReferenceDate() << ";"
	      << bond_code  << ";"
	      << t << ";"
	      << y << ";"
	      << getOneInstrument(j)->assignMaturityBracket(brackets) << ";"
	      << lower << ";"
	      << upper << std::endl;
      to_erase.push_back(bond_code);
    }
  }
  for (int k=0;k<to_erase.size();++k) eraseInstrument(to_erase[k]);
}

void Portfolio::removeOutliers(std::ofstream& outfile){
  std::vector<std::string> to_erase;
  for (int j=0; j < getPortfolioSize() ; ++j){
    double t = getOneInstrument(j)->calculateTimeToMaturity();
    double y = getOneInstrument(j)->getYield();
    if (t<0.25 || t>30.0){
      std::string bond_code = getOneInstrument(j)->getBondCode();
      outfile << getReferenceDate() << ";"
	      << bond_code << ";"
	      << t << ";"
	      << y << std::endl;
      to_erase.push_back(bond_code);
    }
  }
  for (int k=0;k<to_erase.size();++k) eraseInstrument(to_erase[k]);
}


std::tuple<int,double,blitz::Array<double,2> > Portfolio::fitBondDiscountCurve(std::ofstream& outfile, blitz::Array<double,2>& x, const int& level){

  auto aa = std::make_pair(1.0,4.0);
  auto bb = std::make_pair(4.0,7.0);
  std::vector<std::pair<double,double> > domain;
  domain.push_back(aa);
  domain.push_back(bb);
  Grid grid(level,domain);
  const int p_size = (0.5 / grid.getDelta() ) + 1;
  blitz::Array<double,1> hcp(p_size);
  hcp = grid.makeCanonicalArray();
  int num_of_nodes = grid.getNumberOfNodes();  
  blitz::Array<double,2>  domain_nodes(num_of_nodes,2);
  domain_nodes = grid.makeDomainNodes();

  // Calibrate Svensson
    std::vector<double> cost_vec;
    std::vector<std::tuple<double,double,double,double,double,double> > x_opt;
   
    setAll();
    auto matrices = makeAllMatrices(); // P, T, C, W, D
    const auto P = std::get<0>(matrices);
    const auto T = std::get<1>(matrices);
    const auto C = std::get<2>(matrices);
    const auto W = std::get<3>(matrices);
    const auto D = std::get<4>(matrices);

    const boost::gregorian::date start_date = getReferenceDate();
    const boost::gregorian::date end_date = timehelper::addToDate(start_date, 30, timehelper::YEAR);
    const CurveType curve_type = TermStructure::getCurveTypeFromString("zero");
    const ParametricForm param_form = TermStructureParametric::getParametricFormFromString("Svensson");
    std::vector<int> newton_iter;
    for (int gridRow=0 ; gridRow < domain_nodes.rows() ; gridRow++){
      if( (domain_nodes(gridRow,0) < domain_nodes(gridRow,1))  &&  (domain_nodes(gridRow,0)>0.0)  &&  (domain_nodes(gridRow,1)>0.0)){
	x = x(0,0), x(1,0), x(2,0), x(3,0),domain_nodes(gridRow,0),domain_nodes(gridRow,1);
	std::cout << "==================================== Grid Iteration (" << domain_nodes(gridRow,0) << "," << domain_nodes(gridRow,1) << ")" << std::endl;
	auto newton_step = newtonStep(x,C,W,T,P,start_date,end_date,curve_type,param_form,"ACTUAL_365");
	cost_vec.push_back(std::get<1>(newton_step));
	newton_iter.push_back(std::get<0>(newton_step));
	blitz::Array<double,2> opt(6,1);
	opt = std::get<2>(newton_step);
	x_opt.push_back(std::make_tuple(opt(0,0),opt(0,1),opt(0,2),opt(0,3),opt(0,4),opt(0,5)));
      } // if grid tau1>tau2
    } // for grid
    std::vector<double>::iterator cost_min;
    cost_min = std::min_element(cost_vec.begin(),cost_vec.end());
    
    int idx = std::distance(cost_vec.begin(),cost_min);
  
    blitz::Array<double,2> x_opt_(6,1);
    x_opt_ = std::get<0>(x_opt[idx]),std::get<1>(x_opt[idx]),std::get<2>(x_opt[idx]),std::get<3>(x_opt[idx]),std::get<4>(x_opt[idx]),std::get<5>(x_opt[idx]);
    //std::cout << x_opt_ << std::endl;
    return std::make_tuple(newton_iter[idx],*cost_min,x_opt_);
}

std::tuple<int,double,blitz::Array<double,2> > Portfolio::newtonStep( blitz::Array<double,2>& X,
								      const blitz::Array<double,2>& C,
								      const blitz::Array<double,2>& W,
								      const blitz::Array<double,1>& T,
								      const blitz::Array<double,1>& P,
								      const boost::gregorian::date& start_date,
								      const boost::gregorian::date& end_date,
								      const CurveType& curve_type,
								      const ParametricForm& param_form,
								      const std::string& day_count){


 //=============================== config params
  const int maxiter = 11;
  const double eps = 10e-4;  
  int iter = 0;
  double norm = 1.0;
  // Constraints
  const int num_cons = 1;
  const int num_vars = 4;
  blitz::Array<double,2> E(num_cons,num_vars);
  blitz::Array<double,2> b(num_cons,1);
  E = 1.0, 1.0, 0.0, 0.0;
  b = 0.0;
  // E = 1.0, 1.0, 0.0, 0.0,
  //     1.0, 0.0, 0.0, 0.0,
  //     0.0, 1.0, 0.0, 0.0,
  //   -1.0, 0.0, 0.0, 0.0,
  //     0.0, -1.0, 0.0, 0.0;
  // b = 0.0,0.0,-0.15, 0.15, 0.15;
  // Initial point
  blitz::Array<double,2> x(4,1);
  blitz::Array<double,1> tau(2);
  x = X(0),X(1),X(2),X(3);
  tau = X(4), X(5);
  std::set<int> active;
  double c1 = 0.0001;
  double c2 = 0.9;
  double c = 0.5;
  while (iter < maxiter) {
    std::cout << "---------------------------------- Newton Iteration " << iter << std::endl;
    try {
      //------------------------------------------------------- Active Set Algorithm
      // 1. Calculate Direction
      // 2. If pk==0, do logic to erase a constraint
      //    2.1 Calculate lambdas
      //       2.1.1 If all lambdas >=0 -> end
      //       2.1.2 Find lambda < 0 -> delete constraint
      // 3. If pk!=0, Calculate max_step and add restriction
      //    3.1 if max_step < 1
      //       3.1.1 Find restriction and add it to the working set
      //       3.1.2 xk+1<-xk+ak*pk
      //-------------------------------------------------------
      
      blitz::Array<double,2> XX(6,1);
      XX = x(0), x(2), x(3), x(4), tau(0), tau(1);     
      auto newton = newtonDirection(XX, C, W, T, P, start_date, end_date, curve_type, param_form, day_count);
      blitz::Array<double,2> p = std::get<0>(newton); // Direction
      blitz::Array<double,2> B = std::get<1>(newton); // Modified Hessian
      blitz::Array<double,2> G = std::get<2>(newton); // Gradient
      double F = std::get<3>(newton); // Cost function at xk
      std::cout << "Fk = " << std::fixed << std::setprecision(8) << F << std::endl;
      std::cout << "xk  = " << std::fixed << std::setprecision(4) << x(0) << ", " << x(1) << ", " << x(2) << ", " << x(3) << std::endl;
      std::cout << "gk  = " << std::fixed << std::setprecision(8) << G(0) << ", " << G(1) << ", " << G(2) << ", " << G(3) << std::endl;
      std::cout << "Hk = " << std::fixed << std::setprecision(8) << B << std::endl;
      std::cout << "pk  = " << std::fixed << std::setprecision(8) << p(0) << ", " << p(1) << ", " << p(2) << ", " << p(3) << std::endl;
      blitz::Array<double,1> ee(4);
      ee=0.0;
      interfaceCLAPACK::calculateEigenvalues(B,ee);
      std::cout << "ee  = " << std::fixed << std::setprecision(8) << ee(0) << ", " << ee(1) << ", " << ee(2) << ", " << ee(3) << std::endl;
      
      double d = matrixhelper::directionalDerivative(G,p);
      std::cout << "d= " << d << std::endl;
      
      double a_max = 1.0;
      double a_min = 0.0;
      
      double aj = 2 * ( F -f ) / d; // Nocedal and Wright formula 3.60

      if (aj>a_max) aj = 0.5;
      
      blitz::Array<double,2> xx(4,1);
      xx = x + a*p;
      blitz::Array<double,2> XXX(6,1);
      XXX = xx(0), xx(2), xx(3), xx(4), tau(0), tau(1);    
      auto newton2 = newtonDirection(XXX, C, W, T, P, start_date, end_date, curve_type, param_form, day_count);
      blitz::Array<double,2> g = std::get<2>(newton2);
      double f = std::get<3>(newton2);
      double D = matrixhelper::directionalDerivative(g,p);
      int tries = 0;
      int max_tries = 5;
      double a = aj;

      while ( ) {
	
	blitz::Array<double,2> xx(4,1);
	xx = x + aj*p;
	blitz::Array<double,2> XXX(6,1);
	XXX = xx(0), xx(2), xx(3), xx(4), tau(0), tau(1);    
	auto newtonj = newtonDirection(XXX, C, W, T, P, start_date, end_date, curve_type, param_form, day_count);
	blitz::Array<double,2> gj = std::get<2>(newtonj);
	blitz::Array<double,2> pj = std::get<0>(newtonj);
	double fj = std::get<3>(newtonj);

	if ( fj >= F) {
	  a_max = aj;
	} else {
	  D = matrixhelper::directionalDerivative(gj, pj);
	  if (fabs(D) <= -c2 * d) {
	    a = aj;
	    break;
	  }
	  if ( D * (a_max - a_min) >= 0.0 ) {
	    a_max = a_min;
	  }
	  a_min = aj;
	  
	}

      } // end while ()
      
      
      // while (  (f > F + c1 * a * d)  && tries < max_tries) {
      // 	std::cout << "f(xk+a*pk) (" << f << ") -- (F + c1 * a * d) " << F+c1*a*d << " -- D " << D << " -- (c2*d) " << c2*d << std::endl;  
      // 	a *= c;
      // 	blitz::Array<double,2> xx(4,1);
      // 	xx = x + a*p;
      // 	blitz::Array<double,2> XXX(6,1);
      // 	XXX = xx(0), xx(2), xx(3), xx(4), tau(0), tau(1);    
      // 	auto newton3 = newtonDirection(XXX, C, W, T, P, start_date, end_date, curve_type, param_form, day_count);
      // 	blitz::Array<double,2> g = std::get<2>(newton3);
      // 	f = std::get<3>(newton3);
      // 	D = matrixhelper::directionalDerivative(g,p);
      // 	std::cout << "try (" << tries << ") -- alpha (" << a << ") -- f(" << f << ")" <<  std::endl;
      // 	tries+= 1;
      // }

     
      if (f > F)  {
	break;

      } else if (fabs(f-F)<eps) {
	break;
      } else {
	 x+=a*p;

	 std::cout << "alpha= " << a << std::endl;
	 std::cout << "Fk+1 = " << std::fixed << std::setprecision(8) << f << std::endl;
	 std::cout << "xk+1  = " << std::fixed << std::setprecision(4) << x(0) << ", " << x(1) << ", " << x(2) << ", " << x(3) << std::endl;
	 
      }
      
      // if (f < F ) {
      // 	x+=a*p;
      // 	std::cout << "a= " << a << std::endl;
      // } else if (fabs(f-F)<eps) {
      // 	x+=a*p;
      // 	std::cout << "Fk+1 = " << std::fixed << std::setprecision(8) << f << std::endl;

      // 	break;
      // } else {      
      // 	while ( f <= F + a * d) {
      // 	  a *= c; 
      // 	  blitz::Array<double,2> xx(4,1);
      // 	  xx = x + a * p;
      // 	  blitz::Array<double,2> XXX(6,1);
      // 	  XXX = xx(0), xx(2), xx(3), xx(4), tau(0), tau(1);    
      // 	  auto newton3 = newtonDirection(XXX, C, W, T, P, start_date, end_date, curve_type, param_form, day_count);
      // 	  f = std::get<3>(newton3);
      // 	  blitz::Array<double,2> GG = std::get<2>(newton3);
      // 	  d = 0.0;
      // 	  for (int i=0;i<GG.rows();++i) d+= GG(i)*p(i);
      // 	}
      // 	if (f < F) {
      // 	  x+=a*p;
      // 	  std::cout << "Fk+1 = " << std::fixed << std::setprecision(8) << f << std::endl;
      // 	  std::cout << "alpha= " << a << std::endl;
      // 	} else {
      // 	  std::cout << "Fk+1 = " << std::fixed << std::setprecision(8) << f << std::endl;
      // 	  break;}
      // }


      
      
      // if (active.size() > 0) {

      // 	blitz::Array<double,2> EE(active.size(),E.columns());
      // 	std::set<int>::iterator it;
      // 	int counter = 0;
      // 	for (it = active.begin() ; it != active.end() ; ++it){
      // 	  EE(counter, blitz::Range::all()) = E(*it,blitz::Range::all());
      // 	  counter +=1;
      // 	}
      // 	// Reduced direction
      // 	auto direction = reducedDirection(p,EE,B,G);
      // 	p = std::get<0>(direction);      
      // }     
      
      // std::cout << "pk  = " << std::fixed << std::setprecision(4) << p(0) << ", " << p(1) << ", " << p(2) << ", " << p(3) << std::endl;

      //double a = step_size(active,p,x,E,b);
     
      //std::cout << "Active Constraints =  " << active.size() << std::endl;

       
    } catch (const std::logic_error& error) {
      break;
    }
  iter +=1;
   
  }// end while

  blitz::Array<double,2> x_new_(6,1);
  x_new_ = x(0,0),x(1,0),x(2,0),x(3,0),tau(0),tau(1);
  TermStructureParametric term_structure_new(start_date, end_date, x_new_,param_form,curve_type,"ACTUAL_365");
  blitz::Array<double,1> r_ = makePricesDiff(term_structure_new, C, T);
  double f_ = calcCostFunctionValue(W,r_);
  return std::make_tuple(iter,f_,x_new_);
}

std::tuple<blitz::Array<double,2>,blitz::Array<double,2>, blitz::Array<double,2>, double > Portfolio::newtonDirection(blitz::Array<double,2>& X,
													      const blitz::Array<double,2>& C,
													      const blitz::Array<double,2>& W,
													      const blitz::Array<double,1>& T,
													      const blitz::Array<double,1>& P,
													      const boost::gregorian::date& start_date,
													      const boost::gregorian::date& end_date,
													      const CurveType& curve_type,
													      const ParametricForm& param_form,
													      const std::string& day_count){

  blitz::Array<double,2> x(4,1);
  blitz::Array<double,1> tau(2);
  x = X(0),X(1),X(2),X(3);
  tau = X(4), X(5);
  TermStructureParametric term_structure(start_date,end_date,X,param_form,curve_type,"ACTUAL_365");
  blitz::Array<double,1> R = makePricesDiff(term_structure, C, T);
  double F = calcCostFunctionValue(W,R);
  blitz::Array<double,2> A = term_structure.getJacobianSvensson(T,tau);
  blitz::Array<double,2> J = makeJacobianCostFunction(C,A,T,x);
  blitz::Array<double,2> G = makeGradientCostFunction(J,W,R);
  blitz::Array<double,2> H = makeHessianCostFunction(C,A,J,W,T,R,x);
  blitz::Array<double,2> B = matrixhelper::makeMatrixPosDef(H);
  blitz::Array<double,2> p(4,1);
  p = 0.0;
  try{
    interfaceCLAPACK::SolveLinear(B,p,G);
  } catch ( const std::logic_error& error) {
    std::cout << "Error while solving B*p=G" << std::endl;}
  p*=-1.0;
  return std::make_tuple(p,B,G,F);
}


std::tuple<blitz::Array<double,2>,blitz::Array<double,2>, blitz::Array<double,2> > Portfolio::reducedDirection(blitz::Array<double,2>& p,
													       blitz::Array<double,2>& EE,
													       blitz::Array<double,2>& B,
													       blitz::Array<double,2>& G){

  // Nullspace of EE
  try {
  blitz::Array<double,2> Z = matrixhelper::nullspace(EE);
  blitz::Array<double,2> Z_t = Z.transpose(blitz::secondDim, blitz::firstDim);
  blitz::Array<double,2> BZ(Z.columns(),Z.columns());
  blitz::Array<double,2> gz(Z.columns(),1);
  blitz::Array<double,2> pz(Z.columns(),1);
  BZ = 0.0;
  gz = 0.0;
  pz = 0.0;
  interfaceCLAPACK::MatrixMult(Z_t,B,Z,BZ); // Reduced Hessian
  interfaceCLAPACK::MatrixMult(Z_t,G,gz); // Reduced Gradient
  interfaceCLAPACK::SolveLinear(BZ,pz,gz);
  interfaceCLAPACK::MatrixMult(Z,pz,p);
  return std::make_tuple(p, BZ, gz);
  } catch (std::logic_error& error) {
    std::cout << "Error in reduced direction" << std::endl;}
}



double Portfolio::step_size(std::set<int>& active,
			    blitz::Array<double,2>& p,
			    blitz::Array<double,2>& x,
			    blitz::Array<double,2>& E,
			    blitz::Array<double,2>& b){

  blitz::Array<double,2> Ep(E.rows(),1);
  blitz::Array<double,2> Ex(E.rows(),1);
  blitz::Array<double,2> ga(E.rows(),1);
  Ep = 0.0;
  Ex = 0.0;
  interfaceCLAPACK::MatrixMult(E,p,Ep);
  interfaceCLAPACK::MatrixMult(E,x,Ex);
  ga = (b -Ex) / Ep;
  std::vector<double> max_step_size;

  for (int k=0;k<E.rows();++k)
    if ( !active.contains(k) ) 
      if (Ep(k)< 0.0)
	if (ga(k)>0.0)
	  max_step_size.push_back(ga(k));

  max_step_size.push_back(1.0);
  double a  = *std::min_element(max_step_size.begin(),max_step_size.end());
  
  std::cout << "a= " << a << std::endl;
  std::string out = "g= ";
  std::string out2 = "Ep= ";
  std::string out3 = "Ex= ";
  for (int k=0;k<ga.rows();++k) out += std::to_string(ga(k)) + ", " ;
  for (int k=0;k<Ep.rows();++k) out2 += std::to_string(Ep(k)) + ", " ;
  for (int k=0;k<Ep.rows();++k) out3 += std::to_string(Ex(k)) + ", " ;
  std::cout << std::fixed << std::setprecision(4) << out << std::endl;
  std::cout << std::fixed << std::setprecision(4) << out2 << std::endl;
  std::cout << std::fixed << std::setprecision(4) << out3 << std::endl;


  for (int k=0;k<ga.rows();++k){
    if (Ep(k) > 0.0 && active.contains(k) ){
      active.erase(k);
      std::cout << "Erase constraint " << k << std::endl;
    }
  }
  
  if ( a < 1.0 ) {
  
    for (int k=0;k<ga.rows();++k){
      if (ga(k)==a){
	active.insert(k);
	std::cout << "Insert constraint " << k << std::endl;
      }
    }
   
    return a;

  } else {
    
    return 1.0;
  }

}



