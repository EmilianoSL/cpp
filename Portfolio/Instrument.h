#ifndef INSTRUMENT_H_
#define INSTRUMENT_H_

#include <string>
#include <vector>
#include <memory>

#include "../helpers/timehelper.h"
#include "../helpers/conventionshelper.h"
#include "../TermStructure/TermStructure.h"
#include "Leg.h"

#include <boost/make_shared.hpp> 
#include <boost/shared_ptr.hpp>
#define BOOST_DATE_TIME_NO_LIB
#include <boost/date_time.hpp>

class Leg;

class Instrument {
 public:

  Instrument();

  virtual ~Instrument();

  virtual double calculatePresentValue(const TermStructure& term_structure);

  virtual void calculatePresentValue(const TermStructure& term_structure,
				     double* result);
  virtual double calculateTimeToMaturity();
  virtual double calculateYield();
  virtual double calculateYield(const double& price);
  virtual std::string assignMaturityBracket(const std::vector<std::pair<double,double>>& brackets);
  
  std::string toString() const ;

  virtual double getPrice() const = 0;

  virtual boost::gregorian::date getMaturityDate() const = 0;

  virtual double getDuration() const = 0;

  virtual double getYield() const = 0;

  virtual double getCoupon() const = 0;

  virtual std::string getCountry() const = 0;

  virtual std::string getBondCode() const = 0;

  virtual std::string getDayCountConvention() const  = 0;
  
  virtual boost::shared_ptr<Leg> getLeg() const = 0;

 protected:

 

};
#endif // INSTRUMENT_H
