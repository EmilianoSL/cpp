#include <sstream>
#include <ostream>
#include "OneCashFlowFix.h"
#include <boost/lexical_cast.hpp>

OneCashFlowFix::OneCashFlowFix(
const boost::gregorian::date& payment_date,
const double amount,
const std::string& currency) :
  m_payment_date(payment_date),
  m_amount(amount),
  m_currency(currency)
{}

OneCashFlowFix::~OneCashFlowFix(){}

boost::gregorian::date OneCashFlowFix::getPaymentDate() const {
  return m_payment_date;
}

double OneCashFlowFix::getAmount() const {
  return m_amount;
}

std::string OneCashFlowFix::getCurrency() const {
  return m_currency;
}

std::string OneCashFlowFix::toString() const {
  std::ostringstream out;
  out << "Payment Date: ";
  out << to_iso_extended_string(m_payment_date) << "\r\n";
  out << "Amount: ";
  out << boost::lexical_cast<std::string>(m_amount) << "\r\n";
  out << "Currency: ";
  out << boost::lexical_cast<std::string>(m_currency);
  return out.str();
}
