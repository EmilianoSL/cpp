#ifndef BOND_H_
#define BOND_H_

#include <string>
#include <vector>
#include <memory>

#include "../helpers/timehelper.h"
#include "../helpers/conventionshelper.h"
#include "../TermStructure/TermStructure.h"
#include "../TermStructure/TermStructureFlat.h"
#include "Instrument.h"


#include <boost/shared_ptr.hpp>
#define BOOST_DATE_TIME_NO_LIB
#include <boost/date_time.hpp>

#include <blitz/array.h>

class Leg;

class Bond : public Instrument {
 public:
  Bond(const boost::gregorian::date& reference_date,
       const std::string& bond_code,
       const double price,
       const double accrued_interest,      
       const double coupon,
       const std::string& frequency,
       const std::string& currency,
       const std::string& country,
       const std::string& day_count_convention,
       const boost::gregorian::date& maturity_date,
       const boost::gregorian::date& last_coupon_date);

  virtual ~Bond();


 std::string getFrequency() const;
 double getCoupon() const;
 virtual  double getPrice() const;
 double getAccruedInterest() const;
 std::string getDayCountConvention() const;
 std::string getCurrency() const;
 virtual std::string getCountry() const;
 std::string getBondCode() const;
 boost::gregorian::date getReferenceDate() const;
 virtual boost::gregorian::date getMaturityDate() const;
 boost::gregorian::date getLastCouponDate() const;
 virtual boost::shared_ptr<Leg> getLeg() const ;
 double calculateTimeToMaturity();
 double calculateDuration();
 double calculateYield();
 double calculateYield(const double& price);
 double getYield() const;
 double getDuration() const;
 std::string assignMaturityBracket(const std::vector<std::pair<double,double>>& brackets);


 std::string toString() const;

 virtual void calculatePresentValue(const TermStructure& term_structure,
 				    double* result);

 virtual double calculatePresentValue(const TermStructure& term_structure);

 protected:
 
  double m_coupon;
  std::string  m_frequency;
  double m_price;
  std::string m_currency;
  std::string m_country;
  std::string m_bond_code;
  double m_accrued_interest;
  double m_yield;
  double m_duration;
  std::string m_day_count_convention;
  boost::gregorian::date m_reference_date;
  boost::gregorian::date m_maturity_date;
  boost::gregorian::date m_last_coupon_date;
  boost::shared_ptr<Leg> m_leg;

 



};
#endif // BOND_H_
