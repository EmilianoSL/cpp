#ifndef BUCKET_H_
#define BUCKET_H_

#include <string>
#define BOOST_DATE_TIME_NO_LIB
#include <boost/date_time.hpp>

class Bucket {
public:
Bucket(
  const boost::gregorian::date& date,
  const double& value,
  const double& time);

 virtual ~Bucket();

 boost::gregorian::date getDate() const ;
 double getValue() const;
 double getTime() const;


std::string toString() const;

protected:

boost::gregorian::date m_date;
double m_value;
double m_time;


}; // BUCKET_H_
#endif
