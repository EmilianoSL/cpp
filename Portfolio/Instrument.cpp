#include <sstream>
#include <ostream>


#include "Instrument.h"

Instrument::Instrument(){};

Instrument::~Instrument(){};

double Instrument::calculatePresentValue(const TermStructure& term_structure){
  return calculatePresentValue(term_structure) ;
}

void Instrument::calculatePresentValue(const TermStructure& term_structure,
				       double* result) {
  
   calculatePresentValue(term_structure,result);
}

double Instrument::calculateTimeToMaturity(){
  return calculateTimeToMaturity();
}

double Instrument::calculateYield(){
  return calculateYield();
}

double Instrument::calculateYield(const double& price){
  return calculateYield(price);
}

std::string Instrument::assignMaturityBracket(const std::vector<std::pair<double,double>>& brackets) {
  return assignMaturityBracket(brackets);
}

std::string Instrument::toString() const {
  std::ostringstream out;
  out << toString();
  return out.str();
}

std::string Instrument::getBondCode() const {
  return getBondCode();
}
