#include <sstream>
#include <ostream>

#include "Bond.h"
#include "Leg.h"
#include "OneCashFlowFix.h"
#include "../helpers/timehelper.h"
#include "../helpers/conventionshelper.h"

Bond::Bond(const boost::gregorian::date& reference_date,
	   const std::string& bond_code,
	   const double price,
	   const double coupon,
	   const double accrued_interest,      	  
	   const std::string& frequency,
	   const std::string& currency,
	   const std::string& country,
	   const std::string& day_count_convention,
	   const boost::gregorian::date& maturity_date,
	   const boost::gregorian::date& last_coupon_date) :
  Instrument(),
  m_coupon(coupon/100.0),
  m_frequency(frequency), 
  m_price((price + accrued_interest)/1.0),
  m_currency(currency),
  m_country(country),
  m_bond_code(bond_code),
  m_accrued_interest(accrued_interest),
  m_day_count_convention(day_count_convention),
  m_reference_date(reference_date),
  m_maturity_date(maturity_date),
  m_last_coupon_date(last_coupon_date)
{
  double principal = 100.0;
  double factor = 1.0;

  if ( timehelper::getCouponFreqAsInteger(frequency) != 0){

  std::vector<boost::gregorian::date> dates_vector = 
  timehelper::generateDatesBackward(reference_date,
				    maturity_date,
				    last_coupon_date,
				    timehelper::getCouponFreqAsInteger(frequency),
				    timehelper::getTimeUnitFromFrequency(frequency)); 
  int number_of_fixed_cashflows = dates_vector.size();

  std::vector<double> amounts_vector;
  std::vector<OneCashFlowFix> fixed_cashflows;
  for (int i = 0 ; i < number_of_fixed_cashflows ; i++) {
    amounts_vector.push_back( (coupon/factor) / 
			      timehelper::getCouponFreqAsInteger(frequency));
  }
   amounts_vector[number_of_fixed_cashflows - 1] += principal;
   for (int i = 0 ; i < number_of_fixed_cashflows ; i++) {
     OneCashFlowFix cf(dates_vector[i],amounts_vector[i],currency);
     fixed_cashflows.push_back(cf);
   }
   
   m_leg = boost::shared_ptr<Leg>(new Leg(fixed_cashflows, day_count_convention));
   m_yield = calculateYield();
   m_duration = calculateDuration();

  }
  else {
    std::vector<OneCashFlowFix> fixed_cashflows;
    OneCashFlowFix cf(maturity_date,principal,currency);
    fixed_cashflows.push_back(cf);
    m_leg = boost::shared_ptr<Leg>(new Leg(fixed_cashflows, day_count_convention));
    m_yield = calculateYield();
    m_duration = calculateDuration();
  }

 }


Bond::~Bond(){};


 boost::gregorian::date Bond::getReferenceDate() const {
  return m_reference_date;
}

 std::string Bond::getBondCode() const {
  return m_bond_code;
}

 double Bond::getPrice() const {
  return m_price;
}

 double Bond::getCoupon() const {
  return m_coupon;
}

 double Bond::getAccruedInterest() const {
  return m_accrued_interest;
}


 std::string  Bond::getFrequency() const {
  return m_frequency;
}
 std::string Bond::getCurrency() const {
  return m_currency;
}
 std::string Bond::getCountry() const {
  return m_country;
}


 std::string Bond::getDayCountConvention() const {
  return m_day_count_convention;
}

 boost::gregorian::date Bond::getMaturityDate() const {
  return m_maturity_date;
}

 boost::gregorian::date Bond::getLastCouponDate() const {
  return m_last_coupon_date;
}




 boost::shared_ptr<Leg> Bond::getLeg() const {
 return m_leg;
}

 double Bond::getYield() const {
  return m_yield;
}

 double Bond::getDuration() const {
  return m_duration;
}

std::string Bond::toString() const {
  std::ostringstream out;
  out << "Reference Date: ";
  out <<  to_iso_extended_string(getReferenceDate())  << "\r\n";
  out <<  getLeg()->toString();
  return out.str();
}
  
double Bond::calculatePresentValue(const TermStructure& term_structure) {

  return m_leg->calculatePresentValue(term_structure);

}

  
void Bond::calculatePresentValue(const TermStructure& term_structure,
				 double* result) {

  m_leg->calculatePresentValue(term_structure,result); 
 
}


double Bond::calculateTimeToMaturity() {
  return (getMaturityDate() - getReferenceDate()).days() / 365.0;
}



double Bond::calculateYield() {
  
  double yield     = 0.0001;
  double yield_new = 0.01;
  double eps       = 0.00001;
  int    max_iter  = 50;
  int    iter      = 0;

  const auto cashflows = m_leg->getFixedCashFlows();
  const double price = getPrice();

  const auto start_date = getReferenceDate();
  const auto end_date = timehelper::addToDate(start_date,50,timehelper::YEAR); 
  const std::string str_curve_type = "zero";
  const CurveType curve_type = TermStructure::getCurveTypeFromString(str_curve_type);
  const std::string day_count = getDayCountConvention();


  while (iter < max_iter && fabs(yield - yield_new) > eps){
    iter += 1;
    yield = yield_new;
    TermStructureFlat term_structure(start_date,
  				     end_date,
  				     yield,
  				     curve_type,
  				     day_count);
    
    double f = m_leg->calculatePresentValue(term_structure) - price;
    double g = m_leg->calculatePresentValueDerivative(term_structure);
    
    yield_new = yield - f / g; 
    
  }
  
  return yield;
}

double Bond::calculateYield(const double& price) {
  
  double yield     = 0.0001;
  double yield_new = 0.01;
  double eps       = 0.00001;
  int    max_iter  = 50;
  int    iter      = 0;

  const auto cashflows = m_leg->getFixedCashFlows();

  const auto start_date = getReferenceDate();
  const auto end_date = timehelper::addToDate(start_date,50,timehelper::YEAR); 
  const std::string str_curve_type = "zero";
  const CurveType curve_type = TermStructure::getCurveTypeFromString(str_curve_type);
  const std::string day_count = getDayCountConvention();


  while (iter < max_iter && fabs(yield - yield_new) > eps){
    iter += 1;
    yield = yield_new;
    TermStructureFlat term_structure(start_date,
  				     end_date,
  				     yield,
  				     curve_type,
  				     day_count);
    
    double f = m_leg->calculatePresentValue(term_structure) - price;
    double g = m_leg->calculatePresentValueDerivative(term_structure);
    
    yield_new = yield - f / g; 
    
  }
  
  return yield;
}

double Bond::calculateDuration() {

  const double price = getPrice();
  const double yield = getYield(); 
  const auto cashflows = m_leg->getFixedCashFlows();
  const auto start_date = getReferenceDate();
  const auto end_date = timehelper::addToDate(start_date,50,timehelper::YEAR); 
  const std::string str_curve_type = "zero";
  const CurveType curve_type = TermStructure::getCurveTypeFromString(str_curve_type);
  const std::string day_count = getDayCountConvention();

  TermStructureFlat term_structure(start_date,
				   end_date,
				   yield,
				   curve_type,
				   day_count);

  double duration = m_leg->calculatePresentValueDerivative(term_structure) / -price;

  return duration;
}

std::string Bond::assignMaturityBracket(const std::vector<std::pair<double,double>>& brackets){

  double t = calculateTimeToMaturity();
  std::string bracket = "no bracket";

  for (auto& b : brackets)
    if (t > b.first && t <= b.second)
      bracket = "(" + std::to_string(b.first) + "," + std::to_string(b.second) + "]";  
    
    
  return bracket ;
}
