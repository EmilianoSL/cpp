#ifndef PORTFOLIO_H_
#define PORTFOLIO_H_

#include <string>
#include <vector>
#include <memory>
#include <set>

//#include "../opt/OptimizationMethod.h"
#include "../helpers/Grid.h"
#include "../helpers/timehelper.h"
#include "../helpers/matrixhelper.h"
#include "../helpers/conventionshelper.h"
#include "../helpers/InterfaceCLAPACK.h"
#include "../helpers/conventionshelper.h"
#include "../TermStructure/TermStructure.h"
#include "../TermStructure/TermStructureFlat.h"
#include "../TermStructure/TermStructureParametric.h"
#include "../Market/BondReference.h"
#include "../Market/DailyCash.h"
#include "Bond.h"
#include "Instrument.h"
#include "OneCashFlowFix.h"

#include <blitz/array.h>

#include <boost/make_shared.hpp> 
#include <boost/shared_ptr.hpp>
#define BOOST_DATE_TIME_NO_LIB
#include <boost/date_time.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>

class Instrument;
class Bond;
class OneCashFlowFix;

class Portfolio {

   public:

  Portfolio(const boost::gregorian::date& reference_date);

  Portfolio(const boost::gregorian::date& referefence_date,
	    const std::string& country);
  
  virtual ~Portfolio();

  std::vector<boost::shared_ptr<Instrument> > getPortfolio() const;

  boost::shared_ptr<Instrument> getOneInstrument(size_t i);

  int getPortfolioSize() const ;

  double calculatePresentValue(const TermStructure& term_structure);

  double calculatePresentValue(const TermStructure& term_structure,
 				     double* result);

  blitz::Array<double,1> calculateYield(const blitz::Array<double,1>& prices);

  void setOneBond(const Bond& bond);

  void setPricesVector();

  void setPricesArray();

  void setYieldsArray();

  void setUniqueDatesDaycounts();
  
  void setInstrumentCashflows();

  void setNumberOfUniqueDates();

  void setCurrency(const std::string& currency);

  void setCountry(const std::string& country);

  void eraseOneInstrument(size_t i);

  void eraseInstrument(std::string& isin);

  void setAll();
  
  std::string getCurrency() const ;
 
  std::string getCountry() const;
  
  boost::shared_ptr<Bond> getOneBond(size_t i);

  std::vector<double> getPricesVector() const;

  blitz::Array<double,1> getPricesArray() const;

  blitz::Array<double,1> getYieldsArray() const;

  blitz::Array<double,1> getTimeToMaturityArray() const;

  void setTimeToMaturityArray();

  std::vector<std::vector<OneCashFlowFix> > getInstrumentCashflows() const;

  int getNumberOfUniqueDates() const ;

  std::vector<std::pair<boost::gregorian::date,std::string> > getUniqueDatesDaycounts() const;

  std::string toString() const;
  
  void clearPortfolio();

  void removeOutliers(const std::vector<std::pair<double,double>>& brackets, std::ofstream& outfile);
  void removeOutliers(std::ofstream& outfile);

  double calcCostFunctionValue(const blitz::Array<double,2>& weight_matrix,
			       const blitz::Array<double,1>& prices_diff);

  double calcCostFunctionValueYields(const blitz::Array<double,2>& weight_matrix,
			       const blitz::Array<double,1>& yields_diff);

  
  double calcCostFunctionJacobian(const TermStructure& term_structure);

  blitz::Array<double,1> makeYieldsDiff(const TermStructure& term_structure);
  
  blitz::Array<double,1> makePricesDiff(const TermStructure& term_structure);

  blitz::Array<double,1> makePricesDiff(const TermStructure& term_structure,
					const blitz::Array<double,2>& cashflows,
					const blitz::Array<double,1>& year_fractions);
  
  boost::gregorian::date getReferenceDate() const;

  blitz::Array<double,1> makePricesArray();

  blitz::Array<double,1> makeTimeToMaturityArray();

  blitz::Array<double,1> makeTimeToMaturities();

  blitz::Array<double,1> matrixPresentValue(const TermStructure& term_structure);

  blitz::Array<double,1> matrixPresentValue(const TermStructure& term_structure,
					    const blitz::Array<double,2>& cashflows,
					    const blitz::Array<boost::gregorian::date,2>& dates);
  
  blitz::Array<double,1> matrixPresentValue(const TermStructure& term_structure,
					    const blitz::Array<double,2>& cashflows,
					    const blitz::Array<double,1>& year_fractions);

  blitz::Array<std::string,1> makeMaturityBrackets(const std::vector<std::pair<double,double>>& brackets);

  blitz::Array<double,1> calcAverageYieldByBracket(const std::vector<std::pair<double,double>>& brackets);

  blitz::Array<double,1> calcStdDevYieldByBracket(const std::vector<std::pair<double,double>>& brackets);

  std::map<std::string,std::pair<double,double>> makeMapYieldByBracket(const std::vector<std::pair<double,double>>& brackets);
 
  std::vector<boost::gregorian::date> makeDatesVector();

  blitz::Array<double,2> makeCashFlowMatrix();

  blitz::Array<double,2> makeCashFlowMatrix(const std::vector<std::pair<boost::gregorian::date,std::string> >& dates_vector);

  void setCashFlowMatrix();

  blitz::Array<double,2> getCashFlowMatrix() const;

  blitz::Array<double,2> makeTimeToMaturityMatrix();
  
  // blitz::Array<double,1> makeTimeToMaturityArray();

  blitz::Array<boost::gregorian::date,2> makeCashFlowDatesMatrix();

  blitz::Array<boost::gregorian::date,2> makeCashFlowDatesMatrix(const std::vector<std::pair<boost::gregorian::date,std::string> >& dates_pair);

  void setCashFlowDatesMatrix();

  blitz::Array<boost::gregorian::date,2> getCashFlowDatesMatrix() const;

  blitz::Array<double,2> makeWeightMatrix();
  blitz::Array<double,2> makeWeightMatrixD();

  std::pair<blitz::Array<double,2>,blitz::Array<double,2>> makeMatrixPair();

  std::vector<std::pair<boost::gregorian::date,std::string> > makeUniqueDatesDayCountConventions();

  blitz::Array<double,2> 
    makeJacobianCostFunction(const blitz::Array<double,2>& C,
			     const blitz::Array<double,2>& A,
			     const blitz::Array<double,1>& T,
			     const blitz::Array<double,2>& x);

  blitz::Array<double,2> 
    makeGradientCostFunction(const blitz::Array<double,2>& J,
			     const blitz::Array<double,2>& W,
			     const blitz::Array<double,1>& R);
						  
  blitz::Array<double,2> 
    makeHessianCostFunction(const blitz::Array<double,2>& C,
			    const blitz::Array<double,2>& A,
			    const blitz::Array<double,2>& J,
			    const blitz::Array<double,2>& W,
			    const blitz::Array<double,1>& T,
			    const blitz::Array<double,1>& R,
			    const blitz::Array<double,2>& x);

  std::tuple<blitz::Array<double,1>,
	     blitz::Array<double,1>,
	     blitz::Array<double,2>,
	     blitz::Array<double,2>,
	     blitz::Array<boost::gregorian::date,2> > makeAllMatrices();
  
  blitz::Array<double,1> fitQuadraticToYields();

  blitz::Array<double,1> getPortfolioYields();

  double calcAverageYield();

  double calcStdDevYields();

  std::tuple<int,double,blitz::Array<double,2> > fitBondDiscountCurve(std::ofstream& outfile, blitz::Array<double,2>& x, const int& level);

  std::tuple<int,double,blitz::Array<double,2> > newtonStep( blitz::Array<double,2>& X,
							     const blitz::Array<double,2>& C,
							     const blitz::Array<double,2>& W,
							     const blitz::Array<double,1>& T,
							     const blitz::Array<double,1>& P,
							     const boost::gregorian::date& start_date,
							     const boost::gregorian::date& end_date,
							     const CurveType& curve_type,
							     const ParametricForm& param_form,
							     const std::string& day_count);


  std::tuple<blitz::Array<double,2>,blitz::Array<double,2>, blitz::Array<double,2>, double >  newtonDirection(blitz::Array<double,2>& X,
												      const blitz::Array<double,2>& C,
												      const blitz::Array<double,2>& W,
												      const blitz::Array<double,1>& T,
												      const blitz::Array<double,1>& P,
												      const boost::gregorian::date& start_date,
												      const boost::gregorian::date& end_date,
												      const CurveType& curve_type,
												      const ParametricForm& param_form,
												      const std::string& day_count);
  

  std::tuple<blitz::Array<double,2>,blitz::Array<double,2>, blitz::Array<double,2> > reducedDirection(blitz::Array<double,2>& p,
												      blitz::Array<double,2>& EE,
												      blitz::Array<double,2>& B,
												      blitz::Array<double,2>& G);

  double step_size(std::set<int>& active,
		   blitz::Array<double,2>& p,
		   blitz::Array<double,2>& x,
		   blitz::Array<double,2>& E,
		   blitz::Array<double,2>& b
		   );
  
 private:

  std::vector<boost::shared_ptr<Instrument> > m_portfolio;
  std::vector<boost::shared_ptr<Bond> > m_bonds;
  boost::gregorian::date m_reference_date;
  std::vector<double> m_prices_vector;
  blitz::Array<double,1> m_prices_array;
  blitz::Array<double,1> m_yields_array;
  std::vector<std::vector<OneCashFlowFix> > m_instrument_cashflows;
  int m_number_of_unique_dates;
  std::string m_currency;
  std::string m_country;
  blitz::Array<double,2> m_cashflow_matrix;
  blitz::Array<boost::gregorian::date,2> m_cashflow_matrix_dates;
  blitz::Array<double,1> m_time_to_maturity_array;
  std::vector<std::pair<boost::gregorian::date,std::string> > m_unique_dates_daycounts;
};
#endif // PORTFOLIO_H_
