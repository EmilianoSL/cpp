#include <sstream>
#include <ostream>

#include "Leg.h"
#include "OneCashFlowFix.h"
#include "../helpers/timehelper.h"
#include "../helpers/conventionshelper.h"

Leg::Leg(const std::vector<OneCashFlowFix> fixed_cashflows,
	   const std::string day_count_convention):
  m_fixed_cashflows(fixed_cashflows),
  m_day_count_convention(day_count_convention)
{}

Leg::~Leg(){}

const std::vector<OneCashFlowFix>& Leg::getFixedCashFlows() const{
  return m_fixed_cashflows;
}

const  std::string Leg::getDayCountConvention() const {
  return m_day_count_convention;
}


void Leg::calculatePresentValue(const TermStructure& term_structure,
				double* result){

  double pv = 0.0;
  
  pv = calculatePresentValue(term_structure);

  *result = pv;
} 


double Leg::calculatePresentValue(const TermStructure& term_structure){
  
  double pv = 0.0;

  int number_of_fixed_cashflows = m_fixed_cashflows.size();

  for (int i = 0 ; i < number_of_fixed_cashflows ; i++){
    const OneCashFlowFix& cashflow = m_fixed_cashflows[i];
    double amount = cashflow.getAmount();
    const boost::gregorian::date date = cashflow.getPaymentDate();
    double factor = term_structure.getDiscountFactor(date);
    pv +=  amount * factor ;
  }
  return pv;
}


std::string Leg::toString() const {
  std::ostringstream out;
  int n =  getFixedCashFlows().size();
  for (int i = 0 ; i < n ; i++){ 
    out << "Cashflow Nr. ";
    out << i <<  "\r\n";
    out << getFixedCashFlows()[i].toString();
    out << "\r\n";
  }
  return out.str();
}
  
double Leg::calculatePresentValueDerivative(const TermStructure& term_structure){

  double pv = 0.0;

  int number_of_fixed_cashflows = m_fixed_cashflows.size();

  const std::string day_count_str =  getDayCountConvention();
  
  conventionshelper::DayCountConvention day_count = 
    conventionshelper::getStrDayCountConventionAsEnum(day_count_str);

  const boost::gregorian::date start =
    term_structure.getBuckets().front().getDate();
  for (int i = 0 ; i < number_of_fixed_cashflows ; i++){
    const OneCashFlowFix& cashflow = m_fixed_cashflows[i];
    double amount = cashflow.getAmount();  
    const boost::gregorian::date end = cashflow.getPaymentDate();
    double year_fraction = 
      conventionshelper::calculateYearFraction(start,end,day_count);   
    double factor = term_structure.getDiscountFactor(end);
    pv += -year_fraction * amount * factor ;
  }

  return pv;
}
