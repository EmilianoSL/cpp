#ifndef TERMSTRUCTUREPARAMETRIC_H_
#define TERMSTRUCTUREPARAMETRIC_H_

#include <sstream>
#include <ostream>
#include <string>
#include <vector>
#include <math.h>

#include "../Portfolio/Bucket.h"
#include "../helpers/conventionshelper.h"
#include "../helpers/timehelper.h"
#include "TermStructure.h"

#include <blitz/array.h>

#define BOOST_DATE_TIME_NO_LIB
#include <boost/date_time.hpp>


enum ParametricForm{
NelsonSiegel,
Svensson
  };


class TermStructureParametric : public TermStructure {
 public:
 TermStructureParametric(const boost::gregorian::date& start_date,
			 const boost::gregorian::date& end_date,
			 const blitz::Array<double,2>& parameters,
			 const ParametricForm parametric_form,
        		 const CurveType curve_type,
			 const std::string day_count);

  TermStructureParametric(const boost::gregorian::date& start_date,
			 const boost::gregorian::date& end_date,
			 const blitz::Array<double,2>& parameters,
			 const ParametricForm parametric_form,
        		 const CurveType curve_type,
			 const std::string day_count,
			 std::map<std::pair<boost::gregorian::date,std::string>,double> cached_year_fractions);
 
 virtual ~TermStructureParametric();

 virtual double getZeroRate(const boost::gregorian::date& date) const ;

 virtual double getZeroRate(const boost::gregorian::date& date,
			    const std::string day_count_convention) const ;

 virtual double getZeroRate(const double& time) const ;

 virtual double getDiscountFactor(const double& time) const;

 virtual double getDiscountFactor(const boost::gregorian::date& date) const;

 virtual double getDiscountFactor(const boost::gregorian::date& date,
				  const std::string day_count_convention) const;

 virtual  std::map<std::pair<boost::gregorian::date,std::string>,double> getCachedYearFractions() const;

 const blitz::Array<double,2>& getParameters() const;

 const double& getX0() const;
 const double& getX1() const;
 const double& getX2() const;
 const double& getX3() const;
 const double& getX4() const;
 const double& getX5() const;

 const boost::gregorian::date& getStartDate() const;

 const boost::gregorian::date& getEndDate() const;

 const ParametricForm getParametricForm() const;

 static const ParametricForm getParametricFormFromString(const std::string& parametric_form);

 std::string toString() const;

 const std::vector<Bucket>& getBuckets() const;

private:
 boost::gregorian::date m_start_date;
 boost::gregorian::date m_end_date;
 blitz::Array<double,2> m_parameters;
 std::vector<Bucket> m_buckets;
 double m_x0;
 double m_x1;
 double m_x2;
 double m_x3;
 double m_x4;
 double m_x5;
 ParametricForm m_parametric_form;
 const double getSvenssonZero(const boost::gregorian::date& date) const;
 const double getSvenssonZero(const boost::gregorian::date& date,
			      const std::string day_count_convention) const;
 const double getSvenssonZero(const double& date) const;
 const double getSvenssonFactor(const boost::gregorian::date& date) const;
 const double getSvenssonFactor(const boost::gregorian::date& date,
				const std::string day_count_convention) const;
 const double getSvenssonFactor(const double& time) const;
 const double getSvenssonForward(const boost::gregorian::date& date) const;
 const double getYearFraction(const boost::gregorian::date& date) const;
 const double getYearFraction(const boost::gregorian::date& date,
			      const std::string day_count_convention) const;


};
#endif // TERMSTRUCTUREPARAMETRIC_H_
