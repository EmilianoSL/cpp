
#ifndef TERMSTRUCTURE_H_
#define TERMSTRUCTURE_H_

#include <string>
#include <vector>

#include "../Portfolio/Bucket.h"
#include "../helpers/conventionshelper.h"
#include "../helpers/timehelper.h"

#include <blitz/array.h>

#define BOOST_DATE_TIME_NO_LIB
#include <boost/date_time.hpp>


enum  CurveType{
  zero,
  discount,
  forward
};

class TermStructure {
 public:
 
 TermStructure(CurveType curve_type, std::string day_count) :
  m_curve_type(curve_type), m_day_count_str(day_count){}

   TermStructure(CurveType curve_type,
		 std::string day_count,
		 std::map<std::pair<boost::gregorian::date,std::string>,double> cached_year_fractions) :
  m_curve_type(curve_type),
    m_day_count_str(day_count),
    m_cached_year_fractions(cached_year_fractions){}

  virtual ~TermStructure();

  virtual const std::vector<Bucket>& getBuckets() const = 0;

  virtual double getZeroRate(const boost::gregorian::date& date) const = 0;

  virtual double getZeroRate(const boost::gregorian::date& date,
			     const std::string day_count_convention) const = 0;

  virtual double getZeroRate(const double& time) const = 0;

  virtual double getDiscountFactor(const double& time) const = 0;

  virtual double getDiscountFactor(const boost::gregorian::date& date,
				   const std::string day_count_convention) const = 0;

  virtual double getDiscountFactor(const boost::gregorian::date& date) const = 0;

  virtual CurveType getCurveType() const {return m_curve_type;}

  virtual std::string getDayCountStr() const {return m_day_count_str;}

  virtual std::string toString() const;

  virtual std::map<std::pair<boost::gregorian::date,std::string>,double> getCachedYearFractions() const = 0;

  static const CurveType getCurveTypeFromString(const std::string& curve_type) ;
 
  static const  blitz::Array<double,2> 
    getJacobianSvensson(const blitz::Array<double,1>& time,
			const blitz::Array<double,1>& tau) ;


  //private: // fit sub class


   protected:

   CurveType  m_curve_type;
   std::string m_day_count_str;
   std::map<std::pair<boost::gregorian::date,std::string>,double> m_cached_year_fractions;

};
#endif //TERMSTRUCTURE_H_

