#include <sstream>
#include <ostream>
#include "TermStructureParametric.h"
#include <boost/lexical_cast.hpp>


TermStructureParametric::TermStructureParametric(
			 const boost::gregorian::date& start_date,
			 const boost::gregorian::date& end_date,
			 const blitz::Array<double,2>& parameters,
			 const ParametricForm parametric_form,
			 const CurveType curve_type,
			 const std::string day_count) :
  TermStructure(curve_type,day_count),
  m_start_date(start_date),
  m_end_date(end_date),
  m_parameters(parameters),
  m_parametric_form(parametric_form),
  m_x0(parameters(0)),
  m_x1(parameters(1)),
  m_x2(parameters(2)),
  m_x3(parameters(3)),
  m_x4(parameters(4)),
  m_x5(parameters(5))
{
 Bucket start = Bucket(start_date, 0.0,0.0);
  m_buckets.push_back(start);
}

TermStructureParametric::TermStructureParametric(
			 const boost::gregorian::date& start_date,
			 const boost::gregorian::date& end_date,
			 const blitz::Array<double,2>& parameters,
			 const ParametricForm parametric_form,
			 const CurveType curve_type,
			 const std::string day_count,
			 std::map<std::pair<boost::gregorian::date,std::string>,double> cached_year_fractions) :
  TermStructure(curve_type,day_count, cached_year_fractions),
  m_start_date(start_date),
  m_end_date(end_date),
  m_parameters(parameters),
  m_parametric_form(parametric_form)
{
 Bucket start = Bucket(start_date, 0.0,0.0);
  m_buckets.push_back(start);
}

TermStructureParametric::~TermStructureParametric(){}; 

std::map<std::pair<boost::gregorian::date,std::string>,double> TermStructureParametric::getCachedYearFractions() const { return m_cached_year_fractions;}




double TermStructureParametric::getZeroRate(const boost::gregorian::date& date) const {
  
  const ParametricForm param_form = getParametricForm();

  switch (param_form) {
  case NelsonSiegel:
      return 0.0;
  case Svensson:
       return getSvenssonZero(date);
  default:
    return 0.0;
  }
}

double TermStructureParametric::getZeroRate(const boost::gregorian::date& date,
					    const std::string day_count_convention)
 const {
  
  const ParametricForm param_form = getParametricForm();

  switch (param_form) {
  case NelsonSiegel:
      return 0.0;
  case Svensson:
    return getSvenssonZero(date,day_count_convention);
  default:
    return 0.0;
  }
}



double TermStructureParametric::getZeroRate(const double& time) const {
  
  const ParametricForm param_form = getParametricForm();

  switch (param_form) {
  case NelsonSiegel:
      return 0.0;
  case Svensson:
       return getSvenssonZero(time);
  default:
    return 0.0;
  }
}


double TermStructureParametric::getDiscountFactor(const boost::gregorian::date& date) const {
  
  const ParametricForm param_form = getParametricForm();

  switch (param_form) {
  case NelsonSiegel:
      return 0.0;
  case Svensson:
       return getSvenssonFactor(date);
  default:
    return 0.0;
  }
}

double 
TermStructureParametric::getDiscountFactor(const boost::gregorian::date& date,
					   const std::string day_count_convention)
 const {
  
  const ParametricForm param_form = getParametricForm();

  switch (param_form) {
  case NelsonSiegel:
      return 0.0;
  case Svensson:
    return getSvenssonFactor(date,day_count_convention);
  default:
    return 0.0;
  }
}


double TermStructureParametric::getDiscountFactor(const double& time) const {
  
  const ParametricForm param_form = getParametricForm();

  switch (param_form) {
  case NelsonSiegel:
      return 0.0;
  case Svensson:
       return getSvenssonFactor(time);
  default:
    return 0.0;
  }
}


const boost::gregorian::date& TermStructureParametric::getStartDate() const {
  return m_start_date;
}

const boost::gregorian::date& TermStructureParametric::getEndDate() const {
  return m_end_date;
}


const blitz::Array<double,2>& TermStructureParametric::getParameters() const {
  return m_parameters;
}

const double& TermStructureParametric::getX0() const {
  return m_x0;
}

const double& TermStructureParametric::getX1() const {
  return m_x1;
}

const double& TermStructureParametric::getX2() const {
  return m_x2;
}

const double& TermStructureParametric::getX3() const {
  return m_x3;
}

const double& TermStructureParametric::getX4() const {
  return m_x4;
}

const double& TermStructureParametric::getX5() const {
  return m_x5;
}

const ParametricForm TermStructureParametric::getParametricForm() const {
  return m_parametric_form;
}

const double TermStructureParametric::getSvenssonZero(const boost::gregorian::date& date) const {

  const blitz::Array<double,2>& param = getParameters();

  const double t = getYearFraction(date);

  const double x1 = param(0);

  const double x2 = param(1);

  const double x3 = param(2);

  const double x4 = param(3);

  const double x5 = param(4);

  const double x6 = param(5);

  if (t==0.0) return x1 + x2;

  if (date > getEndDate()) return x1;

  if (date < getStartDate()) return 0.0;

  double zero = x1;
  
  zero += x2 * (   (1 - exp(-t/x5)) / (t/x5) );
  zero += x3 * ( ( (1 - exp(-t/x5)) / (t/x5) ) - exp(-t/x5) );
  zero += x4 * ( ( (1 - exp(-t/x6)) / (t/x6) ) - exp(-t/x6) );

  return zero; 

}

const double 
TermStructureParametric::getSvenssonZero(const boost::gregorian::date& date,
					 const std::string day_count_convention)
 const {
  
  const blitz::Array<double,2>& param = getParameters();
  
  const double x1 = param(0);

  const double x2 = param(1);

  const double x3 = param(2);

  const double x4 = param(3);

  const double x5 = param(4);

  const double x6 = param(5);
  
  const double t = getYearFraction(date,day_count_convention);
  
  if (t==0.0) return x1 + x2;

  if (date > getEndDate()) return x1;

  if (date < getStartDate()) return 0.0;

  double zero = x1;
  
  zero += x2 * (   (1 - exp(-t/x5)) / (t/x5) );
  zero += x3 * ( ( (1 - exp(-t/x5)) / (t/x5) ) - exp(-t/x5) );
  zero += x4 * ( ( (1 - exp(-t/x6)) / (t/x6) ) - exp(-t/x6) );

  return zero; 

}


const double TermStructureParametric::getSvenssonZero(const double& time) const {
  
  const double p_0 = getX0();
  const double p_1 = getX1();
  const double p_2 = getX2();
  const double p_3 = getX3();
  const double p_4 = getX4();
  const double p_5 = getX5();
  
  if (time==0.0) return p_0 + p_1;
  
  if ( time > 70.0 ) return p_0;

  if ( time < 0.0 ) return 0.0;

  double zero = p_0;
  
  zero += p_1 * (   (1 - exp(-time / p_4 )) / ( time/ p_4 ) );
  zero += p_2 * ( ( (1 - exp(-time / p_4 )) / ( time/ p_4 ) ) - exp(-time/ p_4 ) );
  zero += p_3 * ( ( (1 - exp(-time / p_5 )) / ( time/ p_5 ) ) - exp(-time/ p_5 ) );

  return zero;

}


const double TermStructureParametric::getSvenssonFactor(const boost::gregorian::date& date) const {
  
 const double t = getYearFraction(date);
 
 double zero = getSvenssonZero(date);
  
 return exp(zero * -t);

}

const double
TermStructureParametric::getSvenssonFactor(const boost::gregorian::date& date,
					   const std::string day_count_convention) const {

  auto cached_year_fractions = getCachedYearFractions();

  auto key = std::make_pair(date,day_count_convention);

  auto search = cached_year_fractions.find(key);

  if (search != cached_year_fractions.end()){
      const double t = search->second;
      double zero = getSvenssonZero(t);
      return exp(zero * -t);

  } else {
      const double t = getYearFraction(date,day_count_convention);
      double zero = getSvenssonZero(date,day_count_convention);
      return exp(zero * -t);

  }
}

const double TermStructureParametric::getSvenssonFactor(const double& time) const {
 
 return exp( getSvenssonZero(time) * -time);

}



const double TermStructureParametric::getSvenssonForward(const boost::gregorian::date& date) const {
  return 0.0;
}


const ParametricForm TermStructureParametric::getParametricFormFromString( const std::string& parametric_form)  {

  if (parametric_form == "NelsonSiegel") { return NelsonSiegel;}
  else if (parametric_form == "Svensson") { return Svensson;}
  else return Svensson;
}


const  double TermStructureParametric::getYearFraction(const boost::gregorian::date& date)  const {

  const boost::gregorian::date& start_date = getStartDate();
   
  const std::string day_count_str = getDayCountStr(); 
  
  conventionshelper::DayCountConvention day_count = 
    conventionshelper::getStrDayCountConventionAsEnum(day_count_str);
  
  const double t = 
    conventionshelper::calculateYearFraction(start_date,date,day_count);

  return t;

 }

const  double 
TermStructureParametric::getYearFraction(const boost::gregorian::date& date,
					 const std::string day_count_convention)
  const {

  const boost::gregorian::date& start_date = getStartDate();
    
  conventionshelper::DayCountConvention day_count = 
    conventionshelper::getStrDayCountConventionAsEnum(day_count_convention);
  
  const double t = 
    conventionshelper::calculateYearFraction(start_date,date,day_count);

  return t;

 }





std::string TermStructureParametric::toString() const {
   const blitz::Array<double,2>& param = getParameters();
   std::ostringstream out;
   boost::gregorian::date start = TermStructureParametric::getStartDate(); 
   boost::gregorian::date end   = TermStructureParametric::getEndDate();
   out << "Start-Date: " << "\r\n";
   out << to_iso_extended_string(start) << "\r\n";
   out << "End-Date: " << "\r\n";
   out << to_iso_extended_string(end) << "\r\n";
   out << "Parameters: " << "\r\n"; 
   for (int i = 0 ; i < param.rows() ; i++) {
     out << "x"  ;
     out << boost::lexical_cast<std::string>(i+1);
     out << "-> " ;  
     out << param(i) << "\r\n"; ;
   }
    return out.str();
}


const std::vector<Bucket>& TermStructureParametric::getBuckets() const { return m_buckets;}

