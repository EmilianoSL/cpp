#include <sstream>
#include <ostream>
#include "TermStructure.h"
#include <boost/lexical_cast.hpp>


TermStructure::~TermStructure(){};

const CurveType TermStructure::getCurveTypeFromString(const std::string& curve_type){

  if (curve_type == "zero") {
    return zero;}
  else if (curve_type == "discount") {
    return discount;}
  else if (curve_type == "forward") {
    return forward;}
  else return zero;
}


std::string TermStructure::toString() const {
  std::string out;
  const std::vector<Bucket> buckets = getBuckets();
  int n = buckets.size() - 1 ;
  for (int i=0; i <=  n ; i++){
    Bucket bucket = buckets[i];
    out += bucket.toString();
  }
  return out;
}


const  blitz::Array<double,2> 
TermStructure::getJacobianSvensson(
			 const blitz::Array<double,1>& time,
			 const blitz::Array<double,1>& tau)  {
  
   blitz::Array<double,2> J(time.extent(blitz::firstDim),4);

   J = 0.0;

   blitz::firstIndex i;

   for (int i = 0; i < time.extent(blitz::firstDim) ; i++){
     double t = time(i);
     J(i,0) =  1.0;
     J(i,1) = (1-exp(-t/tau(0)))/(t/tau(0));
     J(i,2) = (1-exp(-t/tau(0)))/(t/tau(0))-exp(-t/tau(0));
     J(i,3) = (1-exp(-t/tau(1)))/(t/tau(1))-exp(-t/tau(1));
   }
  return J;
}
