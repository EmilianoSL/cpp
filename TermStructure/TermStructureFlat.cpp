#include <sstream>
#include <ostream>
#include "TermStructureFlat.h"
#include <boost/lexical_cast.hpp>


TermStructureFlat::TermStructureFlat(const boost::gregorian::date& start_date,
				     const boost::gregorian::date& end_date,
				     const double& level,
				     const CurveType curve_type,
				     const std::string day_count):
  TermStructure(curve_type,day_count)
 {
    Bucket start = Bucket(start_date, level,0.0);
    Bucket end   = Bucket(end_date, level,0.0);
    m_buckets.push_back(start);
    m_buckets.push_back(end);
 }

TermStructureFlat::TermStructureFlat(const boost::gregorian::date& start_date,
				     const boost::gregorian::date& end_date,
				     const double& level,
				     const CurveType curve_type,
				     const std::string day_count,
				     std::map<std::pair<boost::gregorian::date,std::string>,double> cached_year_fractions):
  TermStructure(curve_type,day_count, cached_year_fractions)
 {
    Bucket start = Bucket(start_date, level,0.0);
    Bucket end   = Bucket(end_date, level,0.0);
    m_buckets.push_back(start);
    m_buckets.push_back(end);
 }

TermStructureFlat::~TermStructureFlat(){};


std::map<std::pair<boost::gregorian::date,std::string>,double> TermStructureFlat::getCachedYearFractions() const { return m_cached_year_fractions;}

const std::vector<Bucket>& TermStructureFlat::getBuckets() const {
  return m_buckets;
}

double TermStructureFlat::getZeroRate(const boost::gregorian::date& date) const {
  const std::vector<Bucket>& buckets = getBuckets();
  const double level = buckets[0].getValue();
  const boost::gregorian::date start =  buckets[0].getDate();
  const boost::gregorian::date end   =  buckets[1].getDate();
  double ret;
  ret = (start > date) ?  0.0 :
        (end < date)   ?  0.0 : level;
  return ret;
  
}

double TermStructureFlat::getZeroRate(const boost::gregorian::date& date,
				      const std::string day_count_convention) const {
  return 0.0;
}

double TermStructureFlat::getZeroRate(const double& time) const {

  return 0.0;
}

double TermStructureFlat::getDiscountFactor(const boost::gregorian::date& date) const {

  const std::vector<Bucket>& buckets = getBuckets();
  const boost::gregorian::date start =  buckets[0].getDate();
  std::string day_count_str = getDayCountStr(); 
  conventionshelper::DayCountConvention day_count = 
    conventionshelper::getStrDayCountConventionAsEnum(day_count_str);
  
  double year_fraction = conventionshelper::calculateYearFraction(start,date,day_count);
  
  year_fraction *= -1;

  double zero_rate = getZeroRate(date);

  double factor = pow( 1 + zero_rate , year_fraction );

  return factor;
  
}

double 
TermStructureFlat::getDiscountFactor(const boost::gregorian::date& date,
				     const std::string day_count_convention) const {

  const std::vector<Bucket>& buckets = getBuckets();
  const boost::gregorian::date start =  buckets[0].getDate();
  std::string day_count_str = day_count_convention; 
  conventionshelper::DayCountConvention day_count = 
    conventionshelper::getStrDayCountConventionAsEnum(day_count_str);
  
  double year_fraction = conventionshelper::calculateYearFraction(start,date,day_count);
  
  year_fraction *= -1;

  double zero_rate = getZeroRate(date);

  double factor = pow( 1 + zero_rate , year_fraction );

  return factor;
  
}



double TermStructureFlat::getDiscountFactor(const double& time) const {
  return 1.0;
}

 std::string TermStructureFlat::toString() const {
    std::ostringstream out;
    out << TermStructure::toString();
    return out.str();
 }
