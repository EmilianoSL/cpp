#ifndef TERMSTRUCTUREFLAT_H_
#define TERMSTRUCTUREFLAT_H_

#include <string>
#include <vector>
#include "../Portfolio/Bucket.h"
#include "../helpers/conventionshelper.h"
#include "../helpers/timehelper.h"
#include "TermStructure.h"


#define BOOST_DATE_TIME_NO_LIB
#include <boost/date_time.hpp>


class TermStructureFlat : public TermStructure {
 public:
 
  TermStructureFlat(const boost::gregorian::date& start_date,
		    const boost::gregorian::date& end_date,
		    const double& level,
		    const CurveType curve_type,
		    const std::string day_count);

   TermStructureFlat(const boost::gregorian::date& start_date,
		    const boost::gregorian::date& end_date,
		    const double& level,
		    const CurveType curve_type,
		     const std::string day_count,
		     std::map<std::pair<boost::gregorian::date,std::string>,double> cached_year_fractions);
  
 
 virtual ~TermStructureFlat();

 virtual const std::vector<Bucket>& getBuckets() const;
 
 virtual double getZeroRate(const boost::gregorian::date& date) const ;

 virtual double getZeroRate(const boost::gregorian::date& date,
			    const std::string day_count_convention) const ;

 virtual double getZeroRate(const double& time) const ;

 virtual double getDiscountFactor(const boost::gregorian::date& date) const;

virtual double getDiscountFactor(const boost::gregorian::date& date,
				  const std::string day_count_convention) const;


 virtual double getDiscountFactor(const double& time) const;

 virtual std::string toString() const;

 virtual  std::map<std::pair<boost::gregorian::date,std::string>,double> getCachedYearFractions() const;
 
 private:

  std::vector<Bucket> m_buckets;


};
#endif // TERMSTRUCTUREFLAT_H_
